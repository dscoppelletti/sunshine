package it.scoppelletti.sunshine.cognito.jwt;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.KeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Base64;
import java.util.Date;
import java.util.Map;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SigningKeyResolver;
import io.jsonwebtoken.impl.FixedClock;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import it.scoppelletti.sunshine.SpringExt;
import it.scoppelletti.sunshine.cognito.test.Application;

@Slf4j
@ActiveProfiles(SpringExt.PROFILE_TEST)
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class JjwtTest {
	private static final String PROP_TOKENUSE = "token_use";
	private static final String VALUE_TOKENUSE = "id";
			
	@Value("${jjwt.idToken}")
	private String myIdToken;
	
	@Value("${jjwt.kid}")
	private String myKid;
	
	@Value("${jjwt.iss}")
	private String myIssuer;
	
	@Value("${jjwt.n}")
	private String myModulus;
	
	@Value("${jjwt.e}")
	private String myPublicExp;
	
	@Value("${jjwt.now}")
	private long myNow;
	
	public JjwtTest() {
	}
	
	@Test
	public void parser() throws Exception {
		Jws<Claims> jwt;
		JwtParser parser;
		StringBuilder buf;
		LocalDateTime now;
		
		now = LocalDateTime.ofEpochSecond(myNow, 0, ZoneOffset.UTC);
	
		parser = Jwts.parser()
				.setClock(new FixedClock(Date.from(
						now.toInstant(ZoneOffset.UTC))))
				.require(JjwtTest.PROP_TOKENUSE, JjwtTest.VALUE_TOKENUSE)
				.requireIssuer(myIssuer)
				.setSigningKeyResolver(new JjwtTest.KeyResolver(myKid,
						myModulus, myPublicExp));
		
		jwt = parser.parseClaimsJws(myIdToken);
		
		buf = new StringBuilder();
	    for (Map.Entry<String, Object> entry : jwt.getBody().entrySet()) {
	    	if (buf.length() > 0) {
	    		buf.append("; ");
	    	} else {
	    		buf.append('[');
	    	}
	    	
	    	buf.append(entry.getKey())
	    		.append('=')
	    		.append(entry.getValue());
	    }
	    buf.append(']');
	    myLogger.info("claims={}", buf.toString());
	}
	
	@Test
	public void isSigned() throws Exception {	
		boolean isSigned;
		JwtParser parser;
		
		parser = Jwts.parser();
		isSigned = parser.isSigned(myIdToken);
		MatcherAssert.assertThat("isSigned", isSigned);
	}
	
	private static final class KeyResolver implements SigningKeyResolver {
		private final String myKid;
		private final String myModulus;
		private final String myPublicExp;
		
		private KeyResolver(String kid, String modulus, String publicExp) {
			myKid = kid;
			myModulus = modulus;
			myPublicExp = publicExp;
		}
		
		@Override
		@SuppressWarnings("rawtypes")
		public Key resolveSigningKey(JwsHeader header, Claims claims) {
			String alg, kid;
			BigInteger modulus, publicExp;
		    PublicKey key;
		    KeySpec keySpec;
		    KeyFactory keyFactory;
			Base64.Decoder decoder;
			byte[] buf;
			
			alg = header.getAlgorithm();
			MatcherAssert.assertThat(alg, Matchers.equalTo(
					SignatureAlgorithm.RS256.getValue()));
			
			kid = header.getKeyId();
			MatcherAssert.assertThat(kid, Matchers.equalTo(myKid));
			
			// RFC 7518 - section 6.3.1.1
			// Base64url, unsigned integer
			decoder = Base64.getUrlDecoder();
			buf = decoder.decode(myModulus);
			modulus = new BigInteger(1, buf);
			buf = decoder.decode(myPublicExp);
			publicExp = new BigInteger(1, buf);
			
			try {
				keySpec = new RSAPublicKeySpec(modulus, publicExp);
				keyFactory = KeyFactory.getInstance(
						SignatureAlgorithm.RS256.getFamilyName());
				key = keyFactory.generatePublic(keySpec);
			} catch (GeneralSecurityException ex) {
				throw new RuntimeException("Failed to build key.", ex);
			}

			return key;
		}

		@Override
		@SuppressWarnings("rawtypes")
		public Key resolveSigningKey(JwsHeader header, String plaintext) {
			MatcherAssert.assertThat("resolveSigningKey(JwsHeader,String)",
					false);
			return null;
		}	
	}
}
