/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.cognito;

import java.net.URI;
import javax.validation.constraints.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

/**
 * Configuration properties for Amazon Cognito JWT.
 * 
 * @since 1.0.0
 */
@Validated
@ConfigurationProperties(prefix = CognitoJwtProperties.PREFIX)
public class CognitoJwtProperties {

	/**
	 * The name prefix of the properties.
	 */
	public static final String PREFIX = "it.scoppelletti.cognito.jwt";
	
	private URI myIssuer;
	
	/**
	 * Sole constructor.
	 */
	public CognitoJwtProperties() {
	}
	
	/**
	 * Gets the principal that issues the JWTs.
	 * 
	 * @return Value.
	 */
	@NotNull
	public URI getIssuer() {
		return myIssuer;
	}
	
	/**
	 * Sets the principal that issues the JWTs.
	 * 
	 * @param value Value.
	 */
	public void setIssuer(URI value) {
		myIssuer = value;
	}
}
