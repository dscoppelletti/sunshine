/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.cognito;

import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SigningKeyResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import it.scoppelletti.sunshine.cognito.jwt.CognitoSigningKeyResolver;
import it.scoppelletti.sunshine.cognito.jwt.CognitoTokenVerifier;
import it.scoppelletti.sunshine.security.jwt.JwtTokenVerifier;

/**
 * Configuration for Amazon Cognito JWT.
 *
 * @since 1.0.0
 */
@Configuration
public class CognitoJwtConfiguration {

	/**
	 * Name of the bean implementing the {@code RestTemplate} component.
	 */
	public static final String BEAN_RESTTEMPLATE =
			"it-scoppelletti-cognito-jwt-restTemplate";
	
	/**
	 * Name of the bean implementing the {@code JwtTokenVerifier} component.
	 */
	public static final String BEAN_TOKENVERIFIER =
			"it-scoppelletti-cognito-jwt-tokenVerifier";
	/**
	 * Property reporting the use of a token.
	 */
	public static final String PROP_TOKENUSE = "token_use";
	
	/**
	 * Value indicating the use of a token as an ID token.
	 */
	public static final String VALUE_TOKENUSE = "id";
	
	@Autowired
	private CognitoJwtProperties myProps;
	
	/**
	 * Sole constructor.
	 */
	public CognitoJwtConfiguration() {
	}
	
	/**
	 * Builds the {@code JwtTokenVerifier} component.
	 * 
	 * @return The bean.
	 */
	@Bean(name = CognitoJwtConfiguration.BEAN_TOKENVERIFIER)
	public JwtTokenVerifier jwtTokenVerifier() {
		JwtParser parser;
		
		parser = Jwts.parser()
				.require(CognitoJwtConfiguration.PROP_TOKENUSE,
						CognitoJwtConfiguration.VALUE_TOKENUSE)
				.requireIssuer(myProps.getIssuer().toString())
				.setSigningKeyResolver(signingKeyResolver());
		
		return new CognitoTokenVerifier(parser);
	}
	
	/**
	 * Defines the {@code SigningKeyResolver} component.
	 * 
	 * @return The bean.
	 */
	@Bean
	public SigningKeyResolver signingKeyResolver() {
		return new CognitoSigningKeyResolver(myProps.getIssuer());
	}
	
	/**
	 * Defines the {@code RestTemplate} component used for JWT authentication.
	 * 
	 * @param  builder The builder.
	 * @return         The bean.
	 */
	@Bean(name = CognitoJwtConfiguration.BEAN_RESTTEMPLATE)
	@ConditionalOnMissingBean(name = CognitoJwtConfiguration.BEAN_RESTTEMPLATE)
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
}
