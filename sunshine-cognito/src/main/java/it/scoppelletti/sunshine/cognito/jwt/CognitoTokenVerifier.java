/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.cognito.jwt;

import java.util.Map;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtParser;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import it.scoppelletti.sunshine.security.jwt.JwtTokenVerifier;

/**
 * Verifies ID tokens.
 * 
 * @since 1.0.0
 */
public class CognitoTokenVerifier implements JwtTokenVerifier {
	private final JwtParser myParser;
	
	/**
	 * Costructor.
	 * 
	 * @param parser JWT parser.
	 */
	public CognitoTokenVerifier(JwtParser parser) {
		if (parser == null) {
			throw new NullPointerException("Argument parser is null.");
		}
		
		myParser = parser;
	}

	@Override
	public Map<String, Object> verify(String idToken) throws
			AuthenticationException {
		Jws<Claims> jwt;
		
		try {
			jwt = myParser.parseClaimsJws(idToken);
		} catch (RuntimeException ex) {
			throw new BadCredentialsException(ex.getLocalizedMessage(), ex);
		}
		
		return jwt.getBody();
	}
}
