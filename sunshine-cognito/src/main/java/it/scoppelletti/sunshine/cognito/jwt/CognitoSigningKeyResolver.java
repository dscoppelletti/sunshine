/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.cognito.jwt;

import java.math.BigInteger;
import java.net.URI;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SigningKeyResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import it.scoppelletti.sunshine.ApplicationException;
import it.scoppelletti.sunshine.cognito.CognitoJwtConfiguration;

/**
 * Amazon Cognito implementation of the {@code SigningKeyResolver} interface.
 * 
 * @since 1.0.0
 */
@Slf4j
public class CognitoSigningKeyResolver implements SigningKeyResolver {
	
	@Resource(name = CognitoJwtConfiguration.BEAN_RESTTEMPLATE)
	private RestTemplate myRestClient;
	
	private final URI myIssuer;
	private Map<String, Key> myKeys;
	
	/**
	 * Constructor.
	 * 
	 * @param issuer Base url.
	 */
	public CognitoSigningKeyResolver(URI issuer) {
		if (issuer == null) {
			throw new NullPointerException("Argument baseUrl is null.");
		}
		
		myIssuer = issuer;
		myKeys = new HashMap<>();
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public Key resolveSigningKey(JwsHeader header, Claims claims) {
		String kid;
		Key key;
		
		if (header == null) {
			throw new NullPointerException("Argument header is null.");
		}
		
		kid = header.getKeyId();
		
		synchronized (myKeys) {
			key = myKeys.get(kid);
			if (key != null) {
				return key;
			}
			
			// I don't know if or when the key store should be refreshed:
			// If the key is not found, try reload.
			load();
			key = myKeys.get(kid);
		}
		
		if (key == null) {
			throw new ApplicationException.Builder("keyNotFound")
				.messageArguments(kid)
				.prefix(getClass())
				.status(HttpStatus.NOT_FOUND)
				.build();			
		}
		
		return key;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Key resolveSigningKey(JwsHeader header, String plaintext) {
		throw new UnsupportedOperationException("resolveSigningKey");
	}
	
	/**
	 * Loads the key store.
	 */
	private void load() {
		URI uri;
		BigInteger modulus, publicExp;
	    Key key;
	    KeySpec keySpec;
	    KeyFactory keyFactory;
		CognitoSigningKeyListModel resp;
		Base64.Decoder decoder;
		List<CognitoSigningKeyModel> list;
		byte[] buf;
		
		uri = UriComponentsBuilder.fromUri(myIssuer)
				.pathSegment(".well-known", "jwks.json")
				.build().toUri();
		resp = myRestClient.getForObject(uri, CognitoSigningKeyListModel.class);
		myLogger.debug("Get {} = {}.", uri, resp);
		
		list = resp.getKeys();
		if (list == null) {
			throw new NullPointerException("Property keys is null.");
		}
		
		// RFC 7518 - section 6.3.1.1
		// Base64url, unsigned integer
		decoder = Base64.getUrlDecoder();
		
		try {
			keyFactory = KeyFactory.getInstance(
					SignatureAlgorithm.RS256.getFamilyName());
		} catch (NoSuchAlgorithmException ex) {
			throw new ApplicationException.Builder("failure")
				.prefix(getClass())
				.cause(ex).build();
		}
		
		myKeys.clear();
		for (CognitoSigningKeyModel model : list) {
			buf = decoder.decode(model.getModulus());
			modulus = new BigInteger(1, buf);
			buf = decoder.decode(model.getPublicExp());
			publicExp = new BigInteger(1, buf);
			keySpec = new RSAPublicKeySpec(modulus, publicExp);
			
			try {
				key = keyFactory.generatePublic(keySpec);
			} catch (InvalidKeySpecException ex) {
				throw new ApplicationException.Builder("failure")
					.prefix(getClass())
					.cause(ex).build();
			}
			
			myKeys.put(model.getKeyId(), key);
		}
	}
}
