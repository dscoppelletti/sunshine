/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.cognito.jwt;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

/**
 * Model of a signing key.
 * 
 * @since 1.0.0
 */
@ToString
public class CognitoSigningKeyModel {
	private String myKeyId;
	private String myUse;
	private String myKeyType;
	private String myAlg;
	private String myModulus;
	private String myPublicExp;
	
	/**
	 * Sole constructor.
	 */
	public CognitoSigningKeyModel() {
	}
	
	/**
	 * Gets the key identifier.
	 * 
	 * @return A value.
	 */
	@JsonProperty("kid")
	public String getKeyId() {
		return myKeyId;
	}
	
	/**
	 * Sets the key identifier.
	 * 
	 * @param value A value.
	 */
	public void setKeyId(String value) {
		myKeyId = value;
	}
	
	/**
	 * Gets the use of key.
	 * 
	 * @return The value.
	 */
	public String getUse() {
		return myUse;
	}
	
	/**
	 * Sets the use of key.
	 * 
	 * @param value A value.
	 */
	public void setUse(String value) {
		myUse = value;
	}
	
	/**
	 * Gets the key type.
	 * 
	 * @return The value.
	 */
	@JsonProperty("kty")
	public String getKeyType() {
		return myKeyType;
	}
	
	/**
	 * Sets the key type.
	 * 
	 * @param value A value.
	 */
	public void setKeyType(String value) {
		myKeyType = value;
	}
	
	/**
	 * Gets the algorithm.
	 * 
	 * @return The value.
	 */
	public String getAlg() {
		return myAlg;
	}
	
	/**
	 * Sets the algorithm.
	 * 
	 * @param value A value.
	 */
	public void setAlg(String value) {
		myAlg = value;
	}
	
	/**
	 * Gets the modulus.
	 * 
	 * @return The value.
	 */
	@JsonProperty("n")
	public String getModulus() {
		return myModulus;
	}
	
	/**
	 * Sets the modulus.
	 * 
	 * @param value A value.
	 */
	public void setModulus(String value) {
		myModulus = value;
	}
	
	/**
	 * Gets the public exponent.
	 * 
	 * @return The value.
	 */
	@JsonProperty("e")
	public String getPublicExp() {
		return myPublicExp;
	}
	
	/**
	 * Sets the public exponent.
	 * 
	 * @param value A value.
	 */
	public void setPublicExp(String value) {
		myPublicExp = value;
	}
}
