/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * <abbr title="JSON Web Token">JWT</abbr>.
 * 
 * <h4>Claims</h4>
 * 
 * <p><table width="100%" border="1" cellpadding="5">
 * <tbody>
 * <tr>
 *     <th>{@code iss}</th>
 *     <td>In the format <code>https://cognito-idp.{region}.amazonaws.com/{userPoolId}</code>.</td>
 * </tr>
 * <tr>
 * 	   <th>{@code sub}</th>
 *     <td>The UUID of the authenticated user.</td>
 * </tr>
 * <tr>
 * 	   <th>{@code aud}</th>
 *     <td>The {@code client_id} with which the user authenticated.</td>
 * </tr>
 * <tr>
 *     <th>{@code token_use}</th>
 *     <td>It is always {@code "id"} in the case of the ID token.</td>
 * </tr>
 * <tr>
 *     <th>{@code name}</th>
 *     <td>The user code.</td>
 * </tr>
 * </tbody>
 * </table></p>
 * 
 * @see <a href="http://docs.aws.amazon.com/cognito/latest/developerguide/amazon-cognito-user-pools-using-tokens-with-identity-providers.html"
 *      target="_blank">Using Tokens with User Pools</a>
 * @version 1.0.0
 */
package it.scoppelletti.sunshine.cognito.jwt;
