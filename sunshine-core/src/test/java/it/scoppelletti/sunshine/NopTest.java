package it.scoppelletti.sunshine;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import it.scoppelletti.sunshine.SpringExt;
import it.scoppelletti.sunshine.test.Application;

@ActiveProfiles(SpringExt.PROFILE_TEST)
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class NopTest {

	public NopTest() {
	}
	
	@Test
	public void test() throws Exception {
	}
}
