/*
 * Copyright (C) 2016 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.model;

import org.springframework.http.HttpStatus;

/**
 * A base class for modeling exceptions.
 * 
 * @since 1.0.0
 */
public abstract class ExceptionView {
	// - Spring Boot 1.3.4
	// This model class mimics the JSON object returned by the default
	// ErrorController implementation.
	private final long myTimestamp;
	private final String myPath;
	private final String myException;
	private final HttpStatus myStatus;

	/**
	 * Constructor.
	 * 
	 * @param path   The request path.
	 * @param ex     The exception.
	 * @param status The status code.
	 */
	protected ExceptionView(String path, Throwable ex, HttpStatus status) {
		if (ex == null) {
			throw new NullPointerException("Argument ex is null.");
		}
		
		myTimestamp = System.currentTimeMillis();
		myPath = path;
		myException = ex.getClass().getName();
		myStatus = (status == null) ? HttpStatus.INTERNAL_SERVER_ERROR : status;
	}
	
	/**
	 * Gets the status code.
	 * 
	 * @return The value.
	 */
	public int getStatus() {
		return myStatus.value();
	}
	
	/**
	 * Gets the error.
	 * 
	 * @return The value.
	 */
	public String getError() {
		return myStatus.getReasonPhrase();
	}
	
	/**
	 * Gets the exception class.
	 * 
	 * @return The value.
	 */
	public String getException() {
		return myException;
	}
	
	/**
	 * Gets the request path.
	 * 
	 * @return The value.
	 */
	public String getPath() {
		return myPath;
	}
	
	/**
	 * Gets the timestamp represented by a millisecond value that is an offset
	 * from the <dfn>Epoch</dfn>.
	 * 
	 * @return The value.
	 */
	public long getTimestamp() {
		return myTimestamp;
	}
}
