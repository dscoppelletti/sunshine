/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.task.AsyncTaskExecutor;

/**
 * Auto-configuration for asynchronous execution.
 * 
 * @since 1.0.0
 */
@Configuration
@ConditionalOnMissingBean(AsyncTaskExecutor.class)
@ConditionalOnProperty(prefix = AsyncProperties.PREFIX, name = "enabled",
	havingValue = "true", matchIfMissing = false)
@EnableConfigurationProperties(AsyncProperties.class)
@Import(AsyncConfiguration.class)
public class AsyncAutoConfiguration {

	/**
	 * Sole constructor.
	 */
	public AsyncAutoConfiguration() {
	}
}
