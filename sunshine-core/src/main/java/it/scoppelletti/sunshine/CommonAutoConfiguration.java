/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine;

import java.util.Collections;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import it.scoppelletti.sunshine.i18n.JavaTimeConverter;

/**
 * Common uto-configuration.
 * 
 * @since 1.0.0
 */
@Configuration
@Import(MvcConfiguration.class)
public class CommonAutoConfiguration {

	/**
	 * Sole constructor.
	 */
	public CommonAutoConfiguration() {
	}
	
	/**
	 * Customizes the conversion service.
	 * 
	 * @return The factory bean.
	 */
	@Bean
	public ConversionServiceFactoryBean conversionService() {
		ConversionServiceFactoryBean bean;
		
		bean = new ConversionServiceFactoryBean();
		bean.setConverters(Collections.singleton(new JavaTimeConverter()));
		return bean;
	}
	
	/**
	 * Customizes the {@code ObjectMapper} component.
	 * 
	 * @return The bean.
	 */
	@Bean
	public Jackson2ObjectMapperBuilderCustomizer objectMapperCustomizer() {
		return new Jackson2ObjectMapperBuilderCustomizer() {

			@Override
			public void customize(Jackson2ObjectMapperBuilder builder) {
				builder
	            	.serializationInclusion(JsonInclude.Include.NON_NULL)
	                .featuresToDisable(
		            		DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
		            .featuresToDisable(
		            		SerializationFeature.WRITE_ENUMS_USING_TO_STRING)
		            // http://blog.oio.de/2015/06/13/add-support-for-java-8-date-time-api-to-jackson-serialized-rest-web-services
		            //     June 13, 2015
		            .modules(new JavaTimeModule());	            	
			}
		};
	}
}
