/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Configuration for asynchronous execution.
 * 
 * @since 1.0.0
 */
@EnableAsync
@Configuration
public class AsyncConfiguration extends AsyncConfigurerSupport {

	@Autowired
	private AsyncProperties myProps;
	
	/**
	 * Sole constructor.
	 */
	public AsyncConfiguration() {
	}
	
	@Override
	public ThreadPoolTaskExecutor getAsyncExecutor() {
		ThreadPoolTaskExecutor executor;
		
		executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(myProps.getThreadPool().getCorePoolSize());
		executor.setMaxPoolSize(myProps.getThreadPool().getMaxPoolSize());
		// executor.setQueueCapacity(Integer.MAX_VALUE);
		// scheduler.setPoolSize(...);
		executor.setAllowCoreThreadTimeOut(true);
		// executor.setKeepAliveSeconds(60);
		executor.setDaemon(true);
		
		if (!StringUtils.isBlank(myProps.getThreadNamePrefix())) {
			executor.setThreadNamePrefix(myProps.getThreadNamePrefix());
		}
		
		executor.setThreadPriority(myProps.getThreadPriority());
		// executor.setRejectedExecutionHandler(...);
		// scheduler.setRemoveOnCancelPolicy(true);
		// scheduler.setErrorHandler(...);
		executor.setWaitForTasksToCompleteOnShutdown(true);
		executor.setAwaitTerminationSeconds(myProps.getAwaitTermination());
		executor.initialize();
		
		return executor;
	}
}
