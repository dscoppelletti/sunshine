/*
 * Copyright (C) 2016 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import it.scoppelletti.sunshine.model.ExceptionView;

/**
 * A set of a validation errors.
 * 
 * @since 1.0.0
 */
public class ValidationErrorView extends ExceptionView {
	private final String myObjectName;
	private final List<String> myGlobalErrors;
	private final Map<String, ValidationErrorView.FieldModel> myFieldErrors;
	
	/**
	 * Constructor.
	 * 
	 * @param path      The request path.
	 * @param ex        The source exception.
	 * @param locale    The localization.
	 * @param msgSource The {@code MessageSource} bean.
	 */
	public ValidationErrorView(String path, Throwable ex, Errors errors,
			Locale locale, MessageSource msgSource) {
		super(path, ex, HttpStatus.BAD_REQUEST);
		
		String msg;
		
		if (errors == null) {
			throw new NullPointerException("Argument errors is null.");
		}
		if (locale == null) {
			throw new NullPointerException("Argument locale is null.");
		}
		if (msgSource == null) {
			throw new NullPointerException("Argument msgSource is null.");
		}
		
		myObjectName = errors.getObjectName();
		myGlobalErrors = new ArrayList<>();
		myFieldErrors = new HashMap<>();
		
		for (ObjectError err : errors.getAllErrors()) {
			msg = msgSource.getMessage(err, locale);
			if (err instanceof FieldError) {
				addFieldError((FieldError) err, msg);
			} else {
				myGlobalErrors.add(msg);
			}
		}
	}
	
	/**
	 * Gets the name of the bound root object.
	 * 
	 * @return The value.
	 */
	public String getObjectName() {
		return myObjectName;
	}	
	
	/**
	 * Gets the global error list.
	 * 
	 * @return The collection.
	 */
	public List<String> getGlobalErrors() {
		return myGlobalErrors;
	}
	
	/**
	 * Gets the field error map.
	 * 
	 * @return The collection.
	 */
	public Map<String, ValidationErrorView.FieldModel> getFieldErrors() {
		return myFieldErrors;
	}
	
	/**
	 * Adds a field validation error.
	 * 
	 * @param err A validation error.
	 * @param msg A message.
	 */
	private void addFieldError(FieldError err, String msg) {
		String field;
		ValidationErrorView.FieldModel model;
		
		field = err.getField();
		model = myFieldErrors.get(field);
		if (model == null) {
			model = new ValidationErrorView.FieldModel(err.getRejectedValue());
			myFieldErrors.put(field, model);
		}
		
		model.getErrors().add(msg);
	}
	
	/**
	 * A set of validation errors for a field.
	 */
	public static final class FieldModel {
		private final String myRejectedValue;
		private final List<String> myErrors;
		
		/**
		 * Constructor.
		 * 
		 * @param rejectedValue A rejectedValue.
		 */
		private FieldModel(Object rejectedValue) {
			myRejectedValue = String.valueOf(rejectedValue);
			myErrors = new ArrayList<>(1);
		}
		
		/**
		 * Gets the rejected value.
		 * 
		 * @return The value.
		 */
		public String getRejectedValue() {
			return myRejectedValue;
		}
		
		/**
		 * Gets the error list.
		 * 
		 * @return The collection.
		 */
		public List<String> getErrors() {
			return myErrors;
		}
	}
}
