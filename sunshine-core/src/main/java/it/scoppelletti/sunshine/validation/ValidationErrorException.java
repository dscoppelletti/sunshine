/*
 * Copyright (C) 2016 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.validation;

import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ResponseStatus;
import it.scoppelletti.sunshine.ApplicationException;

/**
 * A validation error exception.
 * 
 * @since 1.0.0
 */
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class ValidationErrorException extends RuntimeException {
	// Sometime I have to throw a Validation exception explicitly, but the
	// MethodArgumentNotValidException constructors requires a MethodParameter
	// object and I should use Reflection to get it, so I prefer to use this
	// customized exception class.
	private static final long serialVersionUID = 1L;
	
	/**
	 * @serial The error collections.
	 */
	private Errors myErrors;
	// - Spring Framework 4.3.2
	// The Errors implementations has to be serializable.
	
	/**
	 * Constructor.
	 * 
	 * @param errors A validation error collection.
	 */
	public ValidationErrorException(Errors errors) {
		super(errors.getObjectName(), null, false, true);
		
		myErrors = errors;
	}
	
	/**
	 * If there where any errors, throws a {@code ValidationErrorException}
	 * exception.
	 * 
	 * @param errors A validation error collection.
	 */
	public static void check(Errors errors) throws ValidationErrorException {
		if (errors == null) {
			throw new NullPointerException("Argument errors is null.");
		}
		
		if (errors.hasErrors()) {
			throw new ValidationErrorException(errors);
		}
	}
	
	/**
	 * Gets the error collection.
	 * 
	 * @return The collection.
	 */
	public Errors getErrors() {
		return myErrors;
	}
	
	@Override
	public String getMessage() {
		boolean many;
		StringBuilder buf;
		
		buf = new StringBuilder(ApplicationException.class.getSimpleName())
				.append("(objectName=")
				.append(super.getMessage())
				.append(", errors=[");
		
		many = false;
		for (ObjectError err : myErrors.getAllErrors()) {
			if (many) {
				buf.append(", ");
			}
			
			toString(buf, err);
			many = true;			
		}
		
		return buf.append("])").toString();
	}	
	
	/**
	 * Appends to a string buffer a string representation of a validation error.
	 * 
	 * @param buf A string buffer.
	 * @param err A validation error.
	 */
	private void toString(StringBuilder buf, ObjectError err) {
		boolean many;
		FieldError fieldErr;
		buf.append("(defaultMessage=");
		buf.append(err.getDefaultMessage());
		
		if (err instanceof FieldError) {
			fieldErr = (FieldError) err;
			buf.append(", fieldName=");
			buf.append(fieldErr.getField());
			buf.append(", rejectValue=");
			buf.append(String.valueOf(fieldErr.getRejectedValue()));
		}
		
		buf.append(", codes=[");
		
		many = false;
		for (String code : err.getCodes()) {
			if (many) {
				buf.append(", ");
			}
			
			buf.append(code);
			many = true;
		}
		
		buf.append("], args=");
		many = false;
		for (Object arg : err.getArguments()) {
			if (many) {
				buf.append(", ");
			}
			
			buf.append(String.valueOf(arg));
			many = true;
		}
		
		buf.append("])");
	}
}
