/*
 * Copyright (C) 2016 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Base of validator components for using in MVC binding.
 *
 * @param <T> Class of objects to validate.
 * @since     1.0.0
 */
public abstract class ValidatorAdapter<T> implements Validator {
	private final Class<? extends T> myClass;
	
	/**
	 * Constructor.
	 * 
	 * @param clazz Class of objects to validate.
	 */
	protected ValidatorAdapter(Class<? extends T> clazz) {
		if (clazz == null) {
			throw new NullPointerException("Argument clazz is null.");
		}
		
		myClass = clazz;
	}
	
	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		if (myClass.isInstance(target)) {
			doValidate(myClass.cast(target), errors);
		}
	}

	/**
	 * Validates a model.
	 * 
	 * @param model  A model.
	 * @param errors A set into which collect validation errors.
	 */
	protected abstract void doValidate(T model, Errors errors);
}
