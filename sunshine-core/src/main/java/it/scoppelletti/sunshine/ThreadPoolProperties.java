/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine;

import lombok.ToString;

/**
 * Configuration properties for thread pools.
 * 
 * @since 1.0.0
 */
@ToString
public class ThreadPoolProperties {
	private int myCorePoolSize;
	private int myMaxPoolSize;
	
	/**
	 * Sole constructor.
	 */
	public ThreadPoolProperties() {
		myCorePoolSize = 5;
		myMaxPoolSize = 10;
	}
	
	/**
	 * Gets the core pool size.
	 * 
	 * @return The value.
	 */
	public int getCorePoolSize() {
		return myCorePoolSize;
	}
	
	/**
	 * Sets the core pool size.
	 * 
	 * @param value A value.
	 */
	public void setCorePoolSize(int value) {
		myCorePoolSize = value;
	}
	
	/**
	 * Gets the maximum pool size.
	 * 
	 * @return The value.
	 */
	public int getMaxPoolSize() {
		return myMaxPoolSize;
	}
	
	/**
	 * Sets the maximum pool size.
	 * 
	 * @param value A value.
	 */
	public void setMaxPoolSize(int value) {
		myMaxPoolSize = value;
	}
}
