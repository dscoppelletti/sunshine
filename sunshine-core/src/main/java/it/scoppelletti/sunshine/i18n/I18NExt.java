/*
 * Copyright (C) 2016 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.i18n;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * I18N operations.
 * 
 * @since 1.0.0
 */
@Slf4j
public final class I18NExt {

	/**
	 * Locale header.
	 */
	public static final String HEADER_LOCALE = "Accept-Language";
	
    /**
     * Time-zone header.
     */
    public static final String HEADER_TIMEZONE = "X-scoppelletti-timezone";
    
	/**
     * UTC time-zone.
     */
    public static final String TZ_UTC = "UTC";
    
    /**
     * UTC time-zone.
     */    
    public static final TimeZone UtcTimeZone =
    		TimeZone.getTimeZone(I18NExt.TZ_UTC);
    
    /**
     * UTC time-zone.
     */
    public static final ZoneId UtcZoneId = ZoneId.of(I18NExt.TZ_UTC);
    
    private static final char TIME_SEP = ':';
    private static final String DATE_PATTERN = "dd/MM/yyyy";
    
	/**
	 * Private constructor for static class.
	 */
	private I18NExt() {
	}
	
	/**
	 * Returns the current date in UTC.
	 * 
	 * @return The value.
	 */	
	public static LocalDate today() {
		return LocalDate.now(I18NExt.UtcZoneId);
	}
	
	/**
	 * Returns the current time in UTC.
	 * 
	 * @return The value.
	 */
	public static LocalDateTime now() {
		return LocalDateTime.now(I18NExt.UtcZoneId);
	}
	
	/**
	 * Returns the client locale.
	 * 
	 * @return The object.
	 */
	public static Locale getClientLocale() {
		return LocaleContextHolder.getLocale();
	}
	
//	/**
//	 * Returns the client time-zone.
//	 * 
//	 * @return The object.
//	 */
//	public static ZoneId getClientZoneId() {
//	    TODO - I would like do not rely only on offset.
//		int offset;
//		TimeZone tz;
//		ZoneId zoneId;
//		ZoneOffset zoneOffset;
//		
//		tz = LocaleContextHolder.getTimeZone();
//		offset = tz.getRawOffset() / 1000;
//		zoneOffset = ZoneOffset.ofTotalSeconds(offset);
//		zoneId = ZoneId.ofOffset(I18NExt.TZ_UTC, zoneOffset);
//		return zoneId;
//	}
	
	/**
	 * Converts a time in UTC time-zone to the corrisponding time in another
	 * time-zone.
	 * 
	 * @param  t      A time in UTC time-zone.
	 * @param  zoneId A time-zone to which convert the time.
	 * @return        The converted time.
	 */
	public static LocalDateTime fromUTC(LocalDateTime t, ZoneId zoneId) {
		ZonedDateTime zoned;
		
		if (t == null) {
			throw new NullPointerException("Argument t is null.");
		}
		if (zoneId == null) {
			throw new NullPointerException("Argument zoneId is null.");
		}
		if (zoneId.equals(I18NExt.UtcZoneId)) {
			return t;
		}
		
		zoned = t.atZone(I18NExt.UtcZoneId);
		zoned = zoned.withZoneSameInstant(zoneId);
		return zoned.toLocalDateTime();
	}
	
	/**
	 * Converts a time in a time-zone to the corrisponding time in UTC
	 * time-zone.
	 * 
	 * @param  t      A time in the specified time-zone.
	 * @param  zoneId A time-zone from which convert the time.
	 * @return        The converted time.
	 */
	public static LocalDateTime toUTC(LocalDateTime t, ZoneId zoneId) {
		ZonedDateTime zoned;
		
		if (t == null) {
			throw new NullPointerException("Argument t is null.");
		}
		if (zoneId == null) {
			throw new NullPointerException("Argument zoneId is null.");
		}
		if (zoneId.equals(I18NExt.UtcZoneId)) {
			return t;
		}
		
		zoned = t.atZone(zoneId);
		zoned = zoned.withZoneSameInstant(I18NExt.UtcZoneId);
		return zoned.toLocalDateTime();
	}
	
	/**
	 * Converts a {@code LocalDate} object to a {@code Date} object.
	 * 
	 * @param  d The original date.
	 * @return   The converted time.
	 */
	public static Date toDate(LocalDate d) {
		LocalDateTime t;
		
		if (d == null) {
			return null;
		}
		
		t = d.atStartOfDay();
		return Date.from(Instant.from(t.atZone(I18NExt.UtcZoneId)));
	}
	
	/**
	 * Converts a {@code LocalDateTime} object to a {@code Date} object.
	 * 
	 * @param  h The original time.
	 * @return   The converted time.
	 */
	public static Date toDate(LocalTime h) {
		LocalDateTime t;
		
		if (h == null) {
			return null;
		}
		
		t = h.atDate(I18NExt.today());
		return Date.from(Instant.from(t.atZone(I18NExt.UtcZoneId)));
	}
	
	/**
	 * Converts a {@code LocalDateTime} object to a {@code Date} object.
	 * 
	 * @param  t The original time.
	 * @return   The converted time.
	 */
	public static Date toDate(LocalDateTime t) {
		if (t == null) {
			return null;
		}
		
		return Date.from(Instant.from(t.atZone(I18NExt.UtcZoneId)));
	}
	
	/**
	 * Creates a decimal number formatter.
	 * 
	 * @param  locale       A locale.
	 * @param  scale        Scale of the values.
	 * @param  groupingUsed Whether the digit grouping is enabled.
	 * @return              The new object.
	 */
	public static NumberFormat createDecimalFormat(Locale locale, int scale,
			boolean groupingUsed) {
		NumberFormat fmt;
		DecimalFormat decimalFmt;
		
		fmt = NumberFormat.getInstance(locale);
		if (!(fmt instanceof DecimalFormat)) {
			myLogger.warn("No DecimalFormat available for locale {}.", locale);
			return fmt;
		}
		
		decimalFmt = (DecimalFormat) fmt;
		decimalFmt.setParseBigDecimal(true);
		decimalFmt.setDecimalSeparatorAlwaysShown(false);
		decimalFmt.setMaximumFractionDigits(scale);
		decimalFmt.setMinimumFractionDigits(scale);
		decimalFmt.setRoundingMode(RoundingMode.HALF_UP);
		decimalFmt.setGroupingUsed(groupingUsed);
		decimalFmt.setGroupingSize(3); 
		return decimalFmt;
	}	
	
	/**
	 * Creates a date formatter.
	 * 
	 * @param  locale A locale.
	 * @return        The new object.
	 */
	public static DateTimeFormatter createDateFormatter(Locale locale) {
		String pattern;
		
		if (locale == null) {
			throw new NullPointerException("Argument locale is null.");
		}
		
		pattern = I18NExt.createDatePattern(locale);
		return DateTimeFormatter.ofPattern(pattern, locale);
	}
	
	/**
	 * Creates a date pattern.
	 * 
	 * @param  locale A locale.
	 * @return        The value.
	 */
	private static String createDatePattern(Locale locale) {
		// I would like only to know the order of the fields year, month and day
		// for the locale, so I could use the DateTimeFormatterBuilder class.
		String pattern, originalPattern;
		DateFormat fmt;
		SimpleDateFormat dateFmt;

		fmt = DateFormat.getDateInstance(DateFormat.SHORT, locale);

		if (!(fmt instanceof SimpleDateFormat)) {
			myLogger.warn("No SimpleDateFormat available for locale {}.",
					locale);
			return I18NExt.DATE_PATTERN;
		}

		dateFmt = (SimpleDateFormat) fmt;
		originalPattern = dateFmt.toPattern();
		if (StringUtils.isEmpty(originalPattern)) {
			myLogger.warn("Empty date format.");
			return I18NExt.DATE_PATTERN;
		}

		pattern = originalPattern;
		if (!pattern.contains("yyyy")) {
			if (pattern.contains("yyy")) {
				pattern = pattern.replace("yyy", "yyyy");
			} else if (pattern.contains("yy")) {
				pattern = pattern.replace("yy", "yyyy");
			} else {
				pattern = pattern.replace("y", "yyyy");
			}
		}

		if (!pattern.contains("MM") && !pattern.contains("MMM")) {
			pattern = pattern.replace("M", "MM");
		}

		if (!pattern.contains("dd") && !pattern.contains("ddd")) {
			pattern = pattern.replace("d", "dd");
		}

		return pattern;
	}	
	
	/**
	 * Creates a time formatter.
	 * 
	 * @param  locale A locale.
	 * @return        The new object.
	 */
	public static DateTimeFormatter createTimeFormatter(Locale locale) {
		if (locale == null) {
			throw new NullPointerException("Argument locale is null.");
		}
		
		// Now the time formatter does not depend from the locale, but I like
		// comply the information hiding principle
		return new DateTimeFormatterBuilder()
				.appendValue(ChronoField.HOUR_OF_DAY, 2)
				.appendLiteral(I18NExt.TIME_SEP)
				.appendValue(ChronoField.MINUTE_OF_HOUR, 2)
				.appendLiteral(I18NExt.TIME_SEP)
				.appendValue(ChronoField.SECOND_OF_MINUTE, 2).toFormatter();
	}		
}
