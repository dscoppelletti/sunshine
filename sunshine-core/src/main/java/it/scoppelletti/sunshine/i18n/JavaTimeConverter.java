/*
 * Copyright (C) 2016 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.i18n;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.GenericConverter;

/**
 * Converter for JSR310 types.
 * 
 * @since 1.0.0
 */
public class JavaTimeConverter implements GenericConverter {

	/**
	 * Sole constructor.
	 */
	public JavaTimeConverter() {
	}
	
	@Override
	public Object convert(Object source, TypeDescriptor sourceType,
			TypeDescriptor targetType) {
		if (source == null) {
			return null;
		}
		
		if (targetType.getType().equals(LocalDate.class)) {
			return LocalDate.parse(source.toString());
		}
		
		if (targetType.getType().equals(LocalDateTime.class)) {
			return LocalDateTime.parse(source.toString());
		}	
		
		return null;
	}

	@Override
	public Set<GenericConverter.ConvertiblePair> getConvertibleTypes() {
		Set<ConvertiblePair> pairs;
		
		pairs = new HashSet<>();
		pairs.add(new GenericConverter.ConvertiblePair(String.class,
				LocalDate.class));
		pairs.add(new GenericConverter.ConvertiblePair(String.class,
				LocalDateTime.class));
		
		return Collections.unmodifiableSet(pairs);
	}
}
