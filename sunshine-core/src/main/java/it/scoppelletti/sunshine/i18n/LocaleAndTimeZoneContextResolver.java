/*
 * Copyright (C) 2016 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.i18n;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.SimpleTimeZoneAwareLocaleContext;
import org.springframework.web.servlet.i18n.AbstractLocaleContextResolver;

/**
 * Resolution of locale and time-zone for a request handling.
 * 
 * @since 1.0.0
 */
@Slf4j
public class LocaleAndTimeZoneContextResolver extends
		AbstractLocaleContextResolver {
	private final List<Locale> myLocales;
	
	/**
	 * Sole constructor.
	 */
	public LocaleAndTimeZoneContextResolver() {
		myLocales = Arrays.asList(Locale.getAvailableLocales());
		setDefaultTimeZone(I18NExt.UtcTimeZone);
	}

	public LocaleContext resolveLocaleContext(HttpServletRequest request) {
		Locale locale;
		TimeZone tz;
		
		locale = resolveLocale(request);
		tz = resolveTimeZone(request);
		
		return new SimpleTimeZoneAwareLocaleContext(locale, tz);
	}

	@Override
	public Locale resolveLocale(HttpServletRequest req) {
		// - Spring Framework 4.2.7
		// AcceptHeadeLocaleResolver does not properly use the Accept-Language
		// header
		
		String s;
		Locale locale;
		List<Locale.LanguageRange> list;
		
		s = req.getHeader(I18NExt.HEADER_LOCALE);
		if (StringUtils.isBlank(s)) {
			return Locale.getDefault();
		}
		
		try {
			list = Locale.LanguageRange.parse(s);
		} catch(IllegalArgumentException ex) {
			myLogger.error(String.format("Failed to parse header %1$s=%2$s.",
					I18NExt.HEADER_LOCALE, s), ex);
			return Locale.getDefault();
		}
		
		locale = Locale.lookup(list, myLocales);
		if (locale == null) {
			myLogger.warn("Cannot resolve header %1$s=%2$s as a locale.",
					I18NExt.HEADER_LOCALE, s);
			return Locale.getDefault();
		}
		
		return locale;
	}
	
	/**
	 * Returns the time-zone set as a request header.
	 * 
	 * @param  req A request.
	 * @return     The time-zone.
	 */
	private TimeZone resolveTimeZone(HttpServletRequest req) {
		String s;
		
		s = req.getHeader(I18NExt.HEADER_TIMEZONE);
		if (StringUtils.isBlank(s)) {
			return getDefaultTimeZone();
		}
		
		return TimeZone.getTimeZone(s);
	}
	
	@Override
	public void setLocaleContext(HttpServletRequest request,
		HttpServletResponse response, LocaleContext localeContext) {
		String s;
		
		if (response.isCommitted()) {
			return;
		}
		
		s = request.getHeader(I18NExt.HEADER_LOCALE);
		if (!StringUtils.isBlank(s)) {
			response.setHeader(I18NExt.HEADER_LOCALE, s);
		}
		
		s = request.getHeader(I18NExt.HEADER_TIMEZONE);
		if (!StringUtils.isBlank(s)) {
			response.setHeader(I18NExt.HEADER_TIMEZONE, s);
		}
	}
}
