/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.security.jwt;

import java.util.Collections;
import java.util.Map;
import org.springframework.security.core.GrantedAuthority;

/**
 * Granted authority that wraps the claims from a JWT.
 * 
 * @see <a href="http://www.iana.org/assignments/jwt/jwt.txt"
 *      target="_blank">JSON Web Token (JWT)</a>
 * @see <a href="http://openid.net/specs/openid-connect-core-1_0.html"
 *      target="_blank">OpenID Connect Core 1.0, section 5.1</a>
 * @since 1.0.0
 */
public final class ClaimsGrantedAuthority implements GrantedAuthority {
	// - Spring Security 4.2.3, Java JWT 0.7.0
	// The AuthenticationPrincipal annotation does not work if the principal is
	// a Claims interface: it seems that is beacause the Claims interface is a
	// Map and is resolved as an instance of the
	// org.springframework.validation.support.BindingAwareModelMap class
	// before the AuthenticationPrincipalArgumentResolver component can act.
	private static final long serialVersionUID = 1L;
	
	/**
	 * The principal that issued the JWT.
	 */
	public static final String PROP_ISSUER = "iss";
	
	/**
	 * The principal that is the subject of the JWT.
	 */
	public static final String PROP_SUBJECT = "sub";
	
	/**
	 * The recipients that the JWT is intended for.
	 */
	public static final String PROP_AUDIENCE = "aud";
	
	/**
	 * The intended purpose of this token.
	 */
	public static final String PROP_TOKENUSE = "token_use";
	
	/**
	 * The expiration time on or after which the JWT must not be accepted for
	 * processing. Number of seconds from the Epoch.
	 */
	public static final String PROP_EXPIRATION = "exp";
	
	/**
	 * The time before which the JWT must not be accepted for processing. Number
	 * of seconds from the Epoch.
	 */
	public static final String PROP_NOTBEFORE = "nbf";
	
	/**
	 * The time at which the JWT was issued. Number of seconds from the Epoch.
	 */
	public static final String PROP_ISSUEDAT = "iat";
	
	/**
	 * The unique identifier for the JWT.
	 */
	public static final String PROP_ID = "jti";
	
	/**
	 * End-User's full name.
	 */
	public static final String PROP_NAME = "name";
	
	/**
	 * Given name(s) or first name(s) of the End-User.
	 */
	public static final String PROP_GIVENNAME = "given_name";
	
	/**
	 * Surname(s) or last name(s) of the End-User.
	 */
	public static final String PROP_FAMILYNAME = "family_name";
	
	/**
	 * Middle name(s) of the End-User.
	 */
	public static final String PROP_MIDDLENAME = "middle_name";
	
	/**
	 * Casual name of the End-User.
	 */
	public static final String PROP_NICKNAME = "nickname";
	
	/**
	 * Shorthand name by which the End-User wishes to be referred.
	 */
	public static final String PROP_PREFERREDUSERNAME = "preferred_username";
	
	/**
	 * URL of the End-User's profile page.
	 */
    public static final String PROP_PROFILE = "profile";
    
    /**
     * URL of the End-User's profile picture. 
     */
    public static final String PROP_PICTURE = "picture";
    
    /**
     * URL of the End-User's Web page or blog.
     */
    public static final String PROP_WEBSITE = "website";
    
    /**
     * End-User's preferred e-mail address.
     * 
     * @see <a href="http://tools.ietf.org/rfc/rfc5322.txt" target="_blank">RFC
     *      5322 - Internet Message Format, Section 3.4</a>
     */
    public static final String PROP_EMAIL = "email";
    
    /**
     * Indicates whether the End-User's e-mail address has been verified.
     */
    public static final String PROP_EMAILVERIFIED = "email_verified";
    
    /**
     * End-User's gender. Should be {@code male} or {@code female}.
     */
    public static final String PROP_GENDER = "gender";
    
    /**
     * End-User's birthday. The format should be {@code YYYY-MM-DD}, 
     * {@code 0000-MM-DD} (the year is omitted) or {@code YYYY} (the month and
     * the day are omitted).
     */
    public static final String PROP_BIRTHDATE = "birthdate";
    
    /**
     * The End-User's time zone.
     * 
     * @see <a href="http://www.twinsun.com/tz/tz-link.htm"
     *      target="_blank">Sources for Time Zone and Daylight Saving Time
     *      Data</a>
     */
    public static final String PROP_ZONEINFO = "zoneinfo";
    
    /**
     * End-User's locale. The format may use a dash {@code "-"} or an underscore
     * {code "_"} as separators.
     * 
     * @see <a href="http://tools.ietf.org/rfc/rfc5646.txt" target="_blank">RFC
     *      5646, BCP 47 - Tags for Identifying Languages</a>
     */
    public static final String PROP_LOCALE = "locale";
    
    /**
     * End-User's preferred telephone number.
     * 
     * @see <a href="http://tools.ietf.org/rfc/rfc3966.txt" target="_blank">RFC
     *      3966 - The tel URI for Telephone Numbers</a>
     */
    public static final String PROP_PHONENUMBER = "phone_number";
    
    /**
     * Indicates whether the End-User's phone number has been verified.
     */
    public static final String PROP_PHONENUMBERVERIFIED =
    		"phone_number_verified";
    
    /**
     * End-User's preferred postal address. 
     * 
     * @see <a href="http://openid.net/specs/openid-connect-core-1_0.html"
     *      href="_blank">OpenID Connect Core 1.0, section 5.1.1</a>
     */
    public static final String PROP_ADDRESS = "address";
    
    /**
     * Time the End-User's information was last updated. Number of seconds from
     * the Epoch.
     */
    public static final String PROP_UPDATEDAT = "updated_at";

	private final Map<String, Object> myClaims;
	
	/**
	 * Constructor.
	 * 
	 * @param claims
	 */
	ClaimsGrantedAuthority(Map<String, Object> claims) {
		if (claims == null) {
			throw new NullPointerException("Argument claims is null.");
		}
		
		myClaims = Collections.unmodifiableMap(claims);
	}
	
	/**
	 * Gets the claims.
	 * 
	 * @return The collection.
	 */
	public Map<String, Object> getClaims() {
		return myClaims;
	}
	
	@Override
	public String getAuthority() {
		return String.valueOf(myClaims.get(ClaimsGrantedAuthority.PROP_ID));
	}
}
