/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.security.jwt;

import org.springframework.security.authentication.AbstractAuthenticationToken;

/**
 * Authentication token.
 * 
 * @since 1.0.0
 */
public class JwtAuthenticationToken extends AbstractAuthenticationToken {
	private static final long serialVersionUID = 1L;

	private String myIdToken;
	
	/**
	 * Constructor for the authentication request.
	 * 
	 * @param idToken Id Token. May be {@code null}.
	 */
	public JwtAuthenticationToken(String idToken) {
		super(null);
		
		myIdToken = idToken;
		setAuthenticated(false);
	}

	@Override
	public Object getPrincipal() {
		return null;
	}
	
	@Override
	public Object getCredentials() {
		return myIdToken;
	}
	
	@Override
	public void setAuthenticated(boolean isAuthenticated) {
		if (isAuthenticated) {
			throw new IllegalArgumentException(
					"Cannot set this token to trusted.");
		}

		super.setAuthenticated(false);
	}
	
	@Override
	public void eraseCredentials() {
		super.eraseCredentials();
		myIdToken = null;
	}
}
