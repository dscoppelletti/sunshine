/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.security.jwt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import it.scoppelletti.sunshine.security.GrantedAuthorityComparator;
import it.scoppelletti.sunshine.security.UserIdGrantedAuthority;

/**
 * JWT implementation of the {@code UserDetails} interface.
 * 
 * @since 1.0.0
 */
public final class JwtUser implements UserDetails {
	private static final long serialVersionUID = 1L;
	
	private final UserIdGrantedAuthority myUserId;
    private final ClaimsGrantedAuthority myClaims;
    private final Collection<GrantedAuthority> myAuthorities;
    
    /**
     * Constructor.
     * 
     * @param claims Collection of claims.
     */
	JwtUser(Map<String, Object> claims) {
		List<GrantedAuthority> authorities;
		
		if (claims == null) {
			throw new NullPointerException("Argument claims is null.");
		}
		
		myUserId = new UserIdGrantedAuthority(String.valueOf(claims.get(
				ClaimsGrantedAuthority.PROP_SUBJECT)));
		myClaims = new ClaimsGrantedAuthority(claims);
		
		authorities = new ArrayList<>();
		authorities.add(myUserId);
		authorities.add(myClaims);
		authorities.sort(GrantedAuthorityComparator.getInstance());
		myAuthorities = Collections.unmodifiableCollection(authorities);
	}
	
	@Override
	public String getUsername() {
		return String.valueOf(myClaims.getClaims().get(
				ClaimsGrantedAuthority.PROP_NAME));
	}

	@Override
	public String getPassword() {
		throw new IllegalStateException();
	}

	@Override
	public boolean isEnabled() {
		long now;
		Long value;

		now = System.currentTimeMillis();
		now = (now - now % 1000L) / 1000L;
		
		value = (Long) myClaims.getClaims().get(
				ClaimsGrantedAuthority.PROP_NOTBEFORE);
		if (value != null && now < value.longValue()) {
			return false;
		}
		
		value = (Long) myClaims.getClaims().get(
				ClaimsGrantedAuthority.PROP_ISSUEDAT);
		if (value != null && now < value.longValue()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		long now;
		Long value;

		now = System.currentTimeMillis();
		now = (now - now % 1000L) / 1000L;
		
		value = (Long) myClaims.getClaims().get(
				ClaimsGrantedAuthority.PROP_EXPIRATION);
		if (value != null && now >= value.longValue()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return myAuthorities;
	}
}
