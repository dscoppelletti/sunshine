/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.security.jwt;

import java.util.Map;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * JWT implementation of the {@code AuthenticationProvider} interface.
 *
 * @since 1.0.0
 */
public class JwtAuthenticationProvider implements AuthenticationProvider {
	private final JwtTokenVerifier myVerifier;
	
	/**
	 * Constructor.
	 * 
	 * @param verifier ID tokens verifier.
	 */
	public JwtAuthenticationProvider(JwtTokenVerifier verifier) {
		if (verifier == null) {
			throw new NullPointerException("Argument verifier is null");
		}
		
		myVerifier = verifier;
	}
	
	@Override
	public Authentication authenticate(Authentication auth)
			throws AuthenticationException {
		String idToken;
		JwtUser principal;
		Authentication user;
		Map<String, Object> claims;
		
		idToken = (String) auth.getCredentials();
		claims = myVerifier.verify(idToken);
		principal = new JwtUser(claims);
		user = new UsernamePasswordAuthenticationToken(principal, null,
				principal.getAuthorities());
		return user;
	}

	@Override
	public boolean supports(Class<?> auth) {
		return JwtAuthenticationToken.class.isAssignableFrom(auth);
	}
}
