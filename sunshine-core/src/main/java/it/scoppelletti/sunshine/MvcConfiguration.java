/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.DefaultMessageCodesResolver;
import org.springframework.validation.MessageCodesResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import it.scoppelletti.sunshine.i18n.LocaleAndTimeZoneContextResolver;
import it.scoppelletti.sunshine.web.MvcExt;

/**
 * Configuration for Spring MVC.
 * 
 * @since 1.0.0
 */
@Configuration
public class MvcConfiguration extends WebMvcConfigurerAdapter {

	/**
	 * Sole constructor.
	 */
	public MvcConfiguration() {
	}
	
	/**
	 * Defines the locale resolver.
	 * 
	 * @return The bean.
	 */
	@Bean
	public LocaleResolver localeResolver() {
		return new LocaleAndTimeZoneContextResolver();
	}
	
	@Override
	public MessageCodesResolver getMessageCodesResolver() {
		DefaultMessageCodesResolver resolver;
		
		resolver = new DefaultMessageCodesResolver();		
		resolver.setPrefix(MvcExt.PREFIX_VALIDATION);
		resolver.setMessageCodeFormatter(
				DefaultMessageCodesResolver.Format.PREFIX_ERROR_CODE);		
		return resolver;
	}

	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		// http://www.java-allandsundry.com/2013/01/spring-mvc-customizing.html
		//     4 January, 2013
		configurer.setUseSuffixPatternMatch(false);
	}
}
