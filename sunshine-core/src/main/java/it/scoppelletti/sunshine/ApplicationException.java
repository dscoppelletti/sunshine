/*
 * Copyright (C) 2008-2016 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.http.HttpStatus;
import it.scoppelletti.sunshine.i18n.I18NExt;

/**
 * Application exception.
 * 
 * @since 1.0.0
 */
public class ApplicationException extends RuntimeException
		implements MessageSourceResolvable {
	private static final long serialVersionUID = 1L;

	/**
	 * @serial The class of which the full qualified name is the prefix to
	 *         prepend the message code.
	 */
	private Class<?> myPrefixClass;
	
	/**
	 * @serial HTTP Status code.
	 */
	private HttpStatus myStatus;

	private transient Object[] myArgs;

	/**
	 * Constructor.
	 * 
	 * @param builder A builder.
	 */
	private ApplicationException(ApplicationException.Builder builder) {
		super(builder.myCode, builder.myCause, false, true);

		myPrefixClass = builder.myPrefixClass;
		myStatus = builder.myStatus;
		myArgs = fixedArguments(builder.myArgs);
	}

	/**
	 * Fixes arguments to be supported by {@code MessageFormat} class.
	 * 
	 * @param  args Original arguments.
	 * @return      Fixed arguments.
	 */
	private Object[] fixedArguments(List<Object> args) {
		int i, n;
		Object arg;
		Object[] v;

		if (args == null) {
			return null;
		}

		n = args.size();
		v = new Object[n];
		for (i = 0; i < n; i++) {
			arg = args.get(i);

			// JDK 8: java.time.* not supported by MessageFormat
			if (arg instanceof LocalDate) {
				arg = I18NExt.toDate((LocalDate) arg);
			} else if (arg instanceof LocalTime) {
				arg = I18NExt.toDate((LocalTime) arg);
			} else if (arg instanceof LocalDateTime) {
				arg = I18NExt.toDate((LocalDateTime) arg);
			}

			v[i] = arg;
		}

		return v;
	}

	/**
	 * Serializes this object.
	 *
	 * @serialData Serialized fields followed by:
	 * 
	 * <ol>
	 * <li>The length of the argument array ({@code int}).</li>
	 * <li>All elements of the argument array (each element an {@code Object})
	 * in the proper order.</li>
	 * </ol>
	 */
	private void writeObject(ObjectOutputStream out) throws IOException {
		int i, n;
		Object obj;

		out.defaultWriteObject();

		n = (myArgs == null) ? 0 : myArgs.length;
		out.writeInt(n);
		for (i = 0; i < n; i++) {
			obj = myArgs[i];
			if (obj instanceof Serializable) {
				out.writeObject(obj);
			} else if (obj == null) {
				out.writeObject(null);
			} else {
				out.writeObject(obj.toString());
			}
		}
	}

	/**
	 * Deserializes this object.
	 */
	private void readObject(ObjectInputStream in)
			throws IOException, ClassNotFoundException {
		int i, n;

		in.defaultReadObject();

		n = in.readInt();
		if (n <= 0) {
			myArgs = null;
		} else {
			myArgs = new Object[n];
			for (i = 0; i < n; i++) {
				myArgs[i] = in.readObject();
			}
		}
	}

	/**
	 * Gets the HTTP status code.
	 * 
	 * @return The value.
	 */
	public HttpStatus getStatus() {
		return myStatus;
	}

	@Override
	public String getMessage() {
		return new StringBuilder(getClass().getSimpleName())
				.append("(code=")
				.append(super.getMessage())
				.append(", prefixClass=")
				.append(myPrefixClass)
				.append(", args=")
				.append(Arrays.deepToString(myArgs))
				.append(")").toString();
	}

	@Override
	public String[] getCodes() {
		int n;
		String code;
		StringBuilder buf;
		List<String> list;
		
		list = new ArrayList<>();
		code = super.getMessage();
		buf = new StringBuilder(getPrefix());
		while (!StringUtils.isEmpty(buf)) {
			n = buf.length();
			buf.append('.').append(code);
			list.add(buf.toString());
			
			buf.setLength(n);
			n = StringUtils.lastIndexOf(buf, '.');
			buf.setLength((n > 0) ? n : 0);
		}
		
		list.add(code);
		return list.toArray(new String[list.size()]);
	}

	/**
	 * Gets the prefix to prepend the message code.
	 * 
	 * @return The value.
	 */
	private String getPrefix() {
		int pos;
		String prefix;
		
		if (myPrefixClass == null) {
			return StringUtils.EMPTY;
		}
		
		prefix = myPrefixClass.getCanonicalName();
		if (!StringUtils.isEmpty(prefix)) {
			// Normal or inner class
			return prefix;
		}
		
		// Anonymous class
		prefix = myPrefixClass.getName();
		pos = prefix.indexOf('$');
		if (pos == 0) {
			return StringUtils.EMPTY;
		} else if (pos > 0) {
			prefix = prefix.substring(0, pos);
		}
		
		return prefix;
	}
	
	@Override
	public Object[] getArguments() {
		return myArgs;
	}

	@Override
	public String getDefaultMessage() {
		return getMessage();
	}

	/**
	 * Builds an {@code ApplicationException} instance.
	 */
	public static final class Builder {
		private final String myCode;
		private Class<?> myPrefixClass;
		private Throwable myCause;
		private HttpStatus myStatus;
		private List<Object> myArgs;

		/**
		 * Constructor.
		 * 
		 * @param code The message code.
		 */
		public Builder(String code) {
			if (StringUtils.isBlank(code)) {
				throw new NullPointerException("Argument code is null.");
			}

			myCode = code;
			myStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		/**
		 * Sets the message arguments.
		 * 
		 * @param  v The array of arguments.
		 * @return   This object.
		 */
		public ApplicationException.Builder messageArguments(Object... v) {
			myArgs = Arrays.asList(v);
			return this;
		}

		/**
		 * Adds a message argument.
		 * 
		 * @param  obj The object. May be {@code null}.
		 * @return     This object.
		 */
		public ApplicationException.Builder addMessageArgument(Object obj) {
			if (myArgs == null) {
				myArgs = new ArrayList<>();
			}

			myArgs.add(obj);
			return this;
		}

		/**
		 * Sets the prefix to prepend the message code.
		 * 
		 * @param  ex The class of which the full qualified name is the prefix.
		 *            May be {@code null}.
		 * @return    This object.
		 */
		public ApplicationException.Builder prefix(Class<?> obj) {
			myPrefixClass = obj;
			return this;
		}
		
		/**
		 * Sets the cause.
		 * 
		 * @param  ex The exception. May be {@code null}.
		 * @return    This object.
		 */
		public ApplicationException.Builder cause(Throwable ex) {
			myCause = ex;
			return this;
		}

		/**
		 * Sets the HTTP status code.
		 * 
		 * @param  value A value.
		 * @return       This object.
		 */
		public ApplicationException.Builder status(HttpStatus value) {
			if (value == null) {
				throw new NullPointerException("Argument value is null.");
			}

			myStatus = value;
			return this;
		}

		/**
		 * Builds a new {@code ApplicationException} instance.
		 * 
		 * @return The new object.
		 */
		public ApplicationException build() {
			return new ApplicationException(this);
		}
	}
}
