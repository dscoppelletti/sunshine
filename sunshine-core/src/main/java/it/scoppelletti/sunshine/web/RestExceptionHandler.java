/*
 * Copyright (C) 2016 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.web;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import it.scoppelletti.sunshine.ApplicationException;
import it.scoppelletti.sunshine.i18n.I18NExt;
import it.scoppelletti.sunshine.model.ApplicationExceptionView;
import it.scoppelletti.sunshine.model.MvcExceptionView;
import it.scoppelletti.sunshine.validation.ValidationErrorException;
import it.scoppelletti.sunshine.validation.ValidationErrorView;

/**
 * Exception handler for REST controllers.
 * 
 * @since 1.0.0
 */
@RestControllerAdvice(annotations = RestController.class)
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private MessageSource myMsgSource;
	
	/**
	 * Sole constructor.
	 */
	public RestExceptionHandler() {
	}
	
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex,
			Object body, HttpHeaders headers, HttpStatus status,
			WebRequest request) {
		String path;
		
		if (body == null) {
			// - Spring Web MVC 4.3.2
			// For some HTTP status the body is null.
			path = MvcExt.getServletPath(request);
			body = new MvcExceptionView(path, ex, status, ex.getMessage());
		}
		
		return super.handleExceptionInternal(ex, body, headers, status,
				request);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		String path;
		ValidationErrorView view;
		
		path = MvcExt.getServletPath(request);
		view = new ValidationErrorView(path, ex, ex.getBindingResult(),
				I18NExt.getClientLocale(), myMsgSource);
		return new ResponseEntity<>(view, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Handles an {@code ApplicationException} exception.
	 * 
	 * @param  req The request.
	 * @param  ex  The exception.
	 * @return     The response entity.
	 */
	@ExceptionHandler(ApplicationException.class)
	public ResponseEntity<?> handleApplicationException(HttpServletRequest req,
			Throwable ex) {
		String msg;
		ApplicationException applEx;
		ApplicationExceptionView view;
		
		applEx = (ApplicationException) ex;
		msg = myMsgSource.getMessage(applEx, I18NExt.getClientLocale());
		view = new ApplicationExceptionView(req.getServletPath(), 				
				(ApplicationException) ex, msg);
		return new ResponseEntity<>(view, applEx.getStatus());
	}
	
	/**
	 * Handles a {@code ValidationErrorException} exception.
	 * 
	 * @param  req The request.
	 * @param  ex  The exception.
	 * @return     The response entity.
	 */
	@ExceptionHandler(ValidationErrorException.class)
	public ResponseEntity<?> handleValidationException(HttpServletRequest req,
			Throwable ex) {
		ValidationErrorException validationEx;
		ValidationErrorView view;
		
		validationEx = (ValidationErrorException) ex;
		view = new ValidationErrorView(req.getServletPath(), ex,
				validationEx.getErrors(), I18NExt.getClientLocale(),
				myMsgSource);
		return new ResponseEntity<>(view, HttpStatus.BAD_REQUEST);
	}
}
