/*
 * Copyright (C) 2016 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.web;

import javax.servlet.http.HttpServletRequest;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

/**
 * Operations on MVC layer.
 * 
 * @since 1.0.0
 */
public final class MvcExt {

	/**
	 * Name of the bean implementing the {@code ConversionService} interface.
	 */
	public static final String BEAN_CONVERSIONSERVICE = "mvcConversionService";
	
	/**
	 * Name of the bean implementing the {@code Validator} interface.
	 */
	public static final String BEAN_VALIDATOR = "mvcValidator";
	
	/**
	 * Prefix of validation message codes.
	 */
	public static final String PREFIX_VALIDATION = "validation.";

	/**
	 * Private constructor for static class.
	 */
	private MvcExt() {
	}
	
	/**
	 * Returns the part of a request's URL that calls the servlet.
	 * 
	 * @param  req A request.
	 * @return     The path.
	 */
	public static String getServletPath(WebRequest req) {
		HttpServletRequest httpReq;
		ServletWebRequest servletReq;
		
		if (req instanceof ServletWebRequest) {
			servletReq = (ServletWebRequest) req;
			httpReq = servletReq.getNativeRequest(HttpServletRequest.class);
			if (httpReq != null) {
				return httpReq.getServletPath();
			}
		}
		
		return req.getContextPath();
	}
}
