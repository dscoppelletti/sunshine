/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine;

import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Configuration properties for asynchronous execution.
 * 
 * @since 1.0.0
 */
@Slf4j
@ToString
@ConfigurationProperties(prefix = AsyncProperties.PREFIX)
public class AsyncProperties implements InitializingBean {
	
	/**
	 * The name prefix of the properties.
	 */
	public static final String PREFIX = "it.scoppelletti.async";
	
	private final ThreadPoolProperties myThreadPool;
	private boolean myEnabled;
	private String myThreadNamePrefix;
	private int myThreadPriority;
	private int myAwaitTermination;
	
	/**
	 * Sole constructor.
	 */
	public AsyncProperties() {
		myEnabled = false;
		myThreadPriority = Thread.NORM_PRIORITY;
		myAwaitTermination = 5;
		myThreadPool = new ThreadPoolProperties();
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		myLogger.info(toString());
	}
	
	/**
	 * Indicates whether asynchronous execution is enabled or disabled.
	 * 
	 * @return Returns {@code true} if asynchronous execution is enabled,
	 *         {@code false} otherwise
	 */
	public boolean isEnabled() {
		return myEnabled;
	}
	
	/**
	 * Sets whether asynchronous execution is enabled or disabled.
	 * 
	 * @param value Whether asynchronous execution is enabled.
	 */
	public void setEnabled(boolean value) {
		myEnabled = value;
	}
	
	/**
	 * Gets the prefix to use for the names of newly created threads.
	 * 
	 * @return The value.
	 */
	public String getThreadNamePrefix() {
		return myThreadNamePrefix;
	}
	
	/**
	 * Sets the prefix to use for the names of newly created threads.
	 * 
	 * @param value A value.
	 */
	public void setThreadNamePrefix(String value) {
		myThreadNamePrefix = value;
	}
	
	/**
	 * Gets the priority of threads.
	 * 
	 * @return The value.
	 */
	public int getThreadPriority() {
		return myThreadPriority;
	}
	
	/**
	 * Sets the proprity of threads.
	 * 
	 * @param value A value (between 1 and 10).
	 */
	public void setThreadPriority(int value) {
		myThreadPriority = value;
	}
	
	/**
	 * Gets the maximum amount of time that this executor may wait for
	 * remaining tasks to complete their execution before the rest of the
	 * container continues to shut down.
	 * 
	 * @return The value (secs).
	 */
	public int getAwaitTermination() {
		return myAwaitTermination;
	}
	
	/**
	 * Sets the maximum amount of time that this executor may wait for
	 * remaining tasks to complete their execution before the rest of the
	 * container continues to shut down.
	 * 
	 * @param value A value (secs).
	 */
	public void setAwaitTermination(int value) {
		myAwaitTermination = value;
	}
	
	/**
	 * Gets the configuration properties for thread pools.
	 * 
	 * @return The object.
	 */
	public ThreadPoolProperties getThreadPool() {
		return myThreadPool;
	}
}
