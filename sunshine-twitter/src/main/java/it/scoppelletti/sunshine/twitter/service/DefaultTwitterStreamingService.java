/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.event.Event;
import com.twitter.hbc.core.processor.HosebirdMessageProcessor;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import it.scoppelletti.sunshine.twitter.TwitterProperties;
import it.scoppelletti.sunshine.twitter.model.TwitterStreamingRequest;

/**
 * Default implementation of the {@code TwitterStreamingService} interface.
 * 
 * @see   <a href="http://github.com/twitter/hbc" target="_blank">Hosebird
 *        Client (hbc)</a>
 * @since 1.0.0
 */
@Slf4j
public class DefaultTwitterStreamingService implements TwitterStreamingService,
		InitializingBean, DisposableBean {

	@Autowired
	private TwitterProperties myProps;
	
	@Autowired
	private TwitterMessageHandler myMessageHandler;
	
	@Autowired
	private TwitterClientEventHandler myEventHandler;
	
	private Client myClient;
	private List<BlockingQueue<String>> myMessageQueues;
	private List<BlockingQueue<Event>> myEventQueues;
	
	/**
	 * Sole constructor.
	 */
	public DefaultTwitterStreamingService() {
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		myMessageQueues = new ArrayList<>();
		myEventQueues = new ArrayList<>();
	}
	
	@Override
	public void destroy() throws Exception {
		disconnect();
	}
	
	@Override
	public void connect(TwitterStreamingRequest params) {
		Hosts hosts;
		ClientBuilder builder;
		Authentication auth;
		StatusesFilterEndpoint endpoint;
		HosebirdMessageProcessor msgProcessor;
		BlockingQueue<String> msgQueue;
		BlockingQueue<Event> eventQueue;
		
		if (params == null) {
			throw new NullPointerException("Argument params is null.");
		}
		
		// Public stream
		hosts = new HttpHosts(Constants.STREAM_HOST);
        
		auth = new OAuth1(myProps.getAuth().getConsumerKey(),
				myProps.getAuth().getConsumerSecret(),
				myProps.getAuth().getAccessToken(),
				myProps.getAuth().getTokenSecret());
	
	    // Filter realtime Tweets
        endpoint = new StatusesFilterEndpoint(true);
        // endpoint.delimited(true);
        
        // - http://developer.twitter.com/en/docs/tweets/filter-realtime/guides/
        //     streaming-message-types - October 13, 2017
        // When connected to a stream using the stall_warnings parameter, you
        // may receive status notices indicating the current health of the
        // connection.
        // endpoint.stallWarnings(true);
        
        if (!CollectionUtils.isEmpty(params.getFollowings())) {
            endpoint.followings(params.getFollowings());
        }
        if (!CollectionUtils.isEmpty(params.getTerms())) {
            endpoint.trackTerms(params.getTerms());
        }
        if (!CollectionUtils.isEmpty(params.getLanguages())) {
            endpoint.languages(params.getLanguages());
        }
        
		msgQueue = new LinkedBlockingQueue<>(
				myProps.getHbc().getMessageQueueSize());
		msgProcessor = new StringDelimitedProcessor(msgQueue);
		eventQueue = new LinkedBlockingQueue<>(
				myProps.getHbc().getEventQueueSize());

		builder = new ClientBuilder()
				.name(getClass().getSimpleName())
				.hosts(hosts)
				.authentication(auth)
				.endpoint(endpoint)
				.processor(msgProcessor)
				.eventMessageQueue(eventQueue);
//				.gzipEnabled(true)
//				.connectionTimeout(4000)
//				.retries(5) // Number of retries to attempt when we experience
					// retryable connection errors
//				.socketTimeout(60000)
		
		disconnect();
		
		myClient = builder.build();
		myClient.connect();	
		myMessageHandler.run(msgQueue);
		myEventHandler.run(eventQueue);
	}
	
	@Override
	public void disconnect() {
		int i;
		BlockingQueue<String> msgQueue;
		BlockingQueue<Event> eventQueue;
		
		for (i = myMessageQueues.size() - 1; i >= 0; i--) {
			msgQueue = myMessageQueues.get(i);
			if (msgQueue.offer(TwitterMessageHandler.MESSAGE_STOP)) {
				myMessageQueues.remove(i);
			} else {
				myLogger.warn("Cannot enqueue stop message.");
			}
		}
		
		for (i = myEventQueues.size() - 1; i >= 0; i--) {
			eventQueue = myEventQueues.get(i);
			if (eventQueue.offer(TwitterClientEventHandler.EVENT_STOP)) {
				myEventQueues.remove(i);
			} else {
				myLogger.warn("Cannot enqueue stop event.");
			}
		}
		
		if (myClient != null) {
			// Waits until the isDone() method returns true, after that, the
			// client cannot be reconnected.
			myClient.stop();
			myClient = null;
		}
	}
}
