/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import lombok.ToString;

/**
 * Symbol that has been parsed out of the text of a Tweet.
 *
 * @see <a href="http://blog.twitter.com/developer/en_us/a/2013/symbols-entities-tweets.html"
 *      target="_blank">Symbols entities for Tweets</a>
 * @since 1.0.0
 */
@ToString(callSuper = true)
public class TwitterSymbolResponse extends TwitterEntityResponse {
	private String myText;
	
	/**
	 * Sole constructor.
	 */
	public TwitterSymbolResponse() {
	}
	
	/**
	 * Gets the text.
	 * 
	 * @return The value.
	 */
	public String getText() {
		return myText;
	}
	
	/**
	 * Sets the text.
	 * 
	 * @param value A value.
	 */
	public void setText(String value) {
		myText = value;
	}
}
