/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

/**
 * URL that has been parsed out of the text of a Tweet.
 *
 * @since 1.0.0
 */
@ToString(callSuper = true)
public class TwitterUrlResponse extends TwitterEntityResponse {
	private String myUrl;
	private String myExpandedUrl;
	private String myDisplayUrl;
	
	/**
	 * Sole constructor.
	 */
	public TwitterUrlResponse() {
	}
	
	/**
	 * Gets the URL.
	 * 
	 * @return The value.
	 */
	public String getUrl() {
		return myUrl;
	}
	
	/**
	 * Sets the URL.
	 * 
	 * @param value A value.
	 */
	public void setUrl(String value) {
		myUrl = value;
	}
	
	/**
	 * Gets the expanded URL.
	 * 
	 * @return The value.
	 */
	@JsonProperty("expanded_url")
	public String getExpandedUrl() {
		return myExpandedUrl;
	}
	
	/**
	 * Sets the expanded URL.
	 * 
	 * @param value A value.
	 */
	public void setExpandedUrl(String value) {
		myExpandedUrl = value;
	}
	
	/**
	 * Gets the display URL.
	 * 
	 * @return The value.
	 */
	@JsonProperty("display_url")
	public String getDisplayUrl() {
		return myDisplayUrl;
	}
	
	/**
	 * Sets the display URL.
	 * 
	 * @param value A value.
	 */
	public void setDisplayUrl(String value) {
		myDisplayUrl = value;
	}
}
