/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.i18n;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.FromStringDeserializer;

/**
 * JSON deserializer for {@code LocalDateTime} fields.
 * 
 * @since 1.0.0
 */
public final class LocalDateTimeDeserializer extends
	FromStringDeserializer<LocalDateTime> {
	private static final long serialVersionUID = 1L;
	
	// Mon Nov 29 21:18:15 +0000 2010
	private static final DateTimeFormatter myFmt =
			DateTimeFormatter.ofPattern("E MMM d H:m:s Z y");
	
	/**
	 * Sole constructor.
	 */
	public LocalDateTimeDeserializer() {
		super(LocalDateTime.class);
	}
	
	@Override
	protected LocalDateTime _deserialize(String value,
			DeserializationContext ctxt) throws IOException {
		LocalDateTime time;
		
		try {
			time = LocalDateTime.parse(value, myFmt);
		} catch (DateTimeParseException ex) {
			throw new IOException(ex.getMessage(), ex);
		}
		
		return time;
	}
}
