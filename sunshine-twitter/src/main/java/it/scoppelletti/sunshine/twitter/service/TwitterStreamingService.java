/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.service;

import it.scoppelletti.sunshine.twitter.model.TwitterStreamingRequest;

/**
 * Filter realtime Tweets service.
 * 
 * @see <a href="http://developer.twitter.com/en/docs/tweets/filter-realtime/overview/statuses-filter"
 *      target="_blank">Filter realtime Tweets</a>
 * @since 1.0.0
 */
public interface TwitterStreamingService {

	/**
	 * Connects to the endpoint and begins streaming.
	 * 
	 * <p>If it is already connect, performs a disconnection and a 
	 * reconnection.</p>
	 * 
	 * @param params Filter parameters.
	 */
	void connect(TwitterStreamingRequest params);
	
	/**
	 * Stops the current connection.
	 */
	void disconnect();
}
