/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import lombok.ToString;

/**
 * Entity that has been parsed out of the text of a Tweet.
 *
 * @since 1.0.0
 */
@ToString
public abstract class TwitterEntityResponse {
	private int[] myIndices;
	
	/**
	 * Sole constructor.
	 */
	protected TwitterEntityResponse() {
	}
	
	/**
	 * Gets the position in the Tweet text.
	 * 
	 * @return The array.
	 */
	public int[] getIndices() {
		return myIndices;
	}
	
	/**
	 * Sets the position in the Tweet text.
	 * 
	 * @param v An array.
	 */
	public void setIndices(int[] v) {
		myIndices = v;
	}
}
