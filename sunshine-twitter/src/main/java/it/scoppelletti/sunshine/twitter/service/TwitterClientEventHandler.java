/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.service;

import java.util.concurrent.BlockingQueue;
import com.twitter.hbc.core.event.Event;
import com.twitter.hbc.core.event.EventType;

/**
 * Handles a client event queue.
 * 
 * @see   it.scoppelletti.sunshine.twitter.service.TwitterStreamingService
 * @since 1.0.0
 */
public interface TwitterClientEventHandler {

	/**
	 * Stop event.
	 */
	public static final Event EVENT_STOP = new Event(EventType.STOPPED_BY_USER);
	
	/**
	 * Handles a client event queue.
	 * 
	 * @param queue A collection.
	 */
	void run(BlockingQueue<Event> queue);
}
