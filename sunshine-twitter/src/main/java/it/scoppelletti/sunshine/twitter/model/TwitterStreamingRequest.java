/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import java.util.ArrayList;
import java.util.List;
import lombok.ToString;

/**
 * Parameters for filtering realtime Tweets.
 * 
 * @see   it.scoppelletti.sunshine.twitter.service.TwitterStreamingService
 * @see   <a href="http://developer.twitter.com/en/docs/tweets/filter-realtime/guides/basic-stream-parameters"
 *        target="_blank">Basic Streaming API request parameters</a>
 * @see   <a href="http://developer.twitter.com/en/docs/tweets/filter-realtime/overview/statuses-filter"
 *        target="_blank">Filter realtime Tweets</a>
 * @since 1.0.0
 */
@ToString
public final class TwitterStreamingRequest {
	private List<Long> myFollowings;
	private List<String> myTerms;
	private List<String> myLangs;
	
	/**
	 * Sole constructor.
	 */
	public TwitterStreamingRequest() {
	}
	
	/**
	 * Gets the list of user IDs, indicating the users whose Tweets should be
	 * delivered on the stream.
	 * 
	 * @return The collection. May be {@code null}.
	 */
	public List<Long> getFollowings() {
		return myFollowings;
	}
	
	/**
	 * Adds a user to follow.
	 * 
	 * <p>The stream will contain:
	 * <ol>
	 * <li>Tweets created by the user</li>
	 * <li>Tweets which are retweeted by the user</li>
	 * <li>Replies to any Tweet created by the user.</li>
	 * <li>Retweets of any Tweet created by the user.</li>
	 * <li>Manual replies, created without pressing a reply button.</li>
	 * </ol></p>
	 * 
	 * <p>The stream will not contain:
	 * <ol>
	 * <li>Tweets mentioning the user.</li>
	 * <li>Manual Retweets created without pressing a Retweet button.</li>
	 * <li>Tweets by protected users.</li>
	 * </ol></p>
	 * 
	 * <p>The default access level allows up to 5000 follow user IDs.</p>
	 * 
	 * @param  userId The user ID.
	 * @return        This object.
	 */
	public TwitterStreamingRequest addFollowing(long userId) {
		if (myFollowings == null) {
			myFollowings = new ArrayList<>();
		}
		
		myFollowings.add(userId);
		return this;
	}
	
	/**
	 * Gets the list of terms which will be used to determine what Tweets will
	 * be delivered on the stream. 
	 * 
	 * @return The collection. May be {@code null}.
	 */
	public List<String> getTerms() {
		return myTerms;
	}
	
	/**
	 * Adds a term to filter what Tweets will be delivered on the stream.
	 * 
	 * <p>The default access level allows up to up to 400 track keywords.</p>
	 * 
	 * @param  term The term. May be a simple word, an hashtag (prefixed by
	 *              {@code "#"} character), a mention (prefixed by
	 *              <code>"&#64;"</code> character).
	 * @return      This object.
	 */
	public TwitterStreamingRequest addTerm(String term) {
		if (myTerms == null) {
			myTerms = new ArrayList<>();
		}
		
		myTerms.add(term);
		return this;
	}
	
	/**
	 * Gets the list of language identifiers, indicating to return only Tweets
	 * that have been detected as being written in the specified languages
	 * 
	 * @return The collection. May be {@code null}.
	 */
	public List<String> getLanguages() {
		return myLangs;
	}
	
	/**
	 * Adds a language to return only Tweets that have been detected as being
	 * written in the specified languages.
	 * 
	 * @param  lang A language code. May be a BCP 47 language identifier or the
	 *              string {@code "und"} to select Tweets that can not be
	 *              detected in which language as being written.
	 * @return      This object.
	 * @see         <a href="http://tools.ietf.org/rfc/bcp/bcp47.txt"
	 *              target="_blank">BCP 74 - Tags for Identifying Languages</a>
	 */
	public TwitterStreamingRequest addLanguage(String lang) {
		if (myLangs == null) {
			myLangs = new ArrayList<>();
		}
		
		myLangs.add(lang);
		return this;
	}
}
