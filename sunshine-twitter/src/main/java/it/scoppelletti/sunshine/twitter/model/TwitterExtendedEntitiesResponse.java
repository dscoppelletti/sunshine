/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import java.util.List;
import lombok.ToString;

/**
 * Extended entities which have been attached to a Tweet.
 * 
 * <p>May be between one and four native photos, or one video, or one animated
 * GIF.</p>
 *
 * @since 1.0.0
 */
@ToString
public class TwitterExtendedEntitiesResponse {
	private List<TwitterMediaResponse> myMedia;

	/**
	 * Sole constructor.
	 */
	public TwitterExtendedEntitiesResponse() {
	}
	
	/**
	 * Gets the media.
	 * 
	 * @return The collection. May be {@code null}.
	 */
	public List<TwitterMediaResponse> getMedia() {
		return myMedia;
	}
	
	/**
	 * Sets the media.
	 * 
	 * @param obj A collection. May be {@code null}.
	 */
	public void setMedia(List<TwitterMediaResponse> obj) {
		myMedia = obj;
	}
}
