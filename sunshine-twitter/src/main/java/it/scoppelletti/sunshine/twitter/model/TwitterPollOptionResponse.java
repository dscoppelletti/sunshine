/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import lombok.ToString;

/**
 * Options in a poll.
 * 
 * @see <a href="http://developer.twitter.com/en/docs/tweets/enrichments/overview/poll-metadata"
 *      target="_blank">Poll metadata</a>
 * @since 1.0.0
 */
@ToString
public class TwitterPollOptionResponse {
	private int myPos;
	private String myText;
	
	/**
	 * Sole constructor.
	 */
	public TwitterPollOptionResponse() {
	}
	
	/**
	 * Gets the value.
	 * 
	 * @return The value (between {@code 1} and {@code 4}).
	 */
	public int getPosition() {
		return myPos;
	}
	
	/**
	 * Sets the position.
	 * 
	 * @param value A value (between {@code 1} and {@code 4}).
	 */
	public void setPosition(int value) {
		myPos = value;
	}
	
	/**
	 * Gets the text.
	 * 
	 * @return The value.
	 */
	public String getText() {
		return myText;
	}
	
	/**
	 * Sets the text.
	 * 
	 * @param value A value.
	 */
	public void setText(String value) {
		myText = value;
	}
}
