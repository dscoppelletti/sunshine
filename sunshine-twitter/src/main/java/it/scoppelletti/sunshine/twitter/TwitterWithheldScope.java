/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.apache.commons.lang3.StringUtils;

/**
 * Withheld scope.
 * 
 * @since 1.0.0
 */
public enum TwitterWithheldScope {

	/**
	 * User.
	 */
	USER,
	
	/**
	 * Status.
	 */
	STATUS;
	
	/**
	 * Converts a string to an enumerated value.
	 * 
	 * @param  name The string. May be {@code null}.
	 * @return      The value. May be {@code null}.
	 */
	@JsonCreator
	public static TwitterWithheldScope fromString(String name) {
		// Waiting for Jackson 2.9.2 that supports the
		// ACCEPT_CASE_INSENSITIVE_ENUMS feature
		return (StringUtils.isBlank(name) ? null :
			TwitterWithheldScope.valueOf(name.toUpperCase()));
	}
}
