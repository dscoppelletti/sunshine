/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.service;

import java.util.concurrent.BlockingQueue;
import com.twitter.hbc.core.event.Event;
import com.twitter.hbc.core.event.EventType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;

/**
 * Default implementation of the {@code TwitterClientEventHandler} interface.
 *
 * @since 1.0.0
 */
@Slf4j
public class DefaultTwitterClientEventHandler implements
		TwitterClientEventHandler {
	
	/**
	 * Sole constructor.
	 */
	public DefaultTwitterClientEventHandler() {
	}
	
	@Async
	@Override
	public void run(BlockingQueue<Event> queue) {
		Event event;
		EventType eventType;
		Exception innerEx;
		
		if (queue == null) {
			throw new NullPointerException("Argument queue is null.");
		}
		
		while (true) {
			try {
				event = queue.take();
			} catch (InterruptedException ex) {
				myLogger.error(ex.getMessage(), ex);
				break;
			}
			
			eventType = event.getEventType();
		    innerEx = event.getUnderlyingException();
			if (innerEx != null) {
				myLogger.error(String.format("eventType=%1$s, message=%1$s",
						eventType, event.getMessage()), innerEx);
				continue;
			}
			
			switch (eventType) {
			case PROCESSING:
				myLogger.trace("eventType={}, message={}", eventType,
						event.getMessage());	
				break;
				
			case CONNECTION_ATTEMPT:
				myLogger.debug("eventType={}, message={}", eventType,
						event.getMessage());	
				break;
				
			case CONNECTED:
			case DISCONNECTED:
			case STOPPED_BY_USER:
				myLogger.info("eventType={}, message={}", eventType,
						event.getMessage());				
				break;
				
			default:
//			case CONNECTION_ERROR:
//			case HTTP_ERROR:
//			case STOPPED_BY_ERROR:
				myLogger.error("eventType={}, message={}", eventType,
						event.getMessage());	
				break;
			}
		}
	}
}
