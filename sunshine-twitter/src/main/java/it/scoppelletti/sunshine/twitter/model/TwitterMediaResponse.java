/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import java.util.Map;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;
import it.scoppelletti.sunshine.twitter.TwitterMediaSize;
import it.scoppelletti.sunshine.twitter.TwitterMediaType;

/**
 * Media that has been parsed out of the text of a Tweet.
 *
 * @see <a href="http://developer.twitter.com/en/docs/tweets/data-dictionary/overview/extended-entities-object"
 *      target="_blank">Twitter Extended Entities</a>
 * @since 1.0.0
 */
@ToString(callSuper = true)
public class TwitterMediaResponse extends TwitterEntityResponse {
	private String myId;
	private String myMediaUrl;
	private String myMediaSecureUrl;
	private String myUrl;
	private String myExpandedUrl;
	private String myDisplayUrl;
	private TwitterMediaType myMediaType;
	private Map<TwitterMediaSize, TwitterMediaSizeResponse> mySizes;
	private TwitterVideoInfoResponse myVideoInfo;
	
	/**
	 * Sole constructor.
	 */
	public TwitterMediaResponse() {
	}
	
	/**
	 * Gets the ID.
	 * 
	 * @return The value.
	 */
	@JsonProperty("id_str")
	public String getId() {
		return myId;
	}
	
	/**
	 * Sets the ID.
	 * 
	 * @param value A value.
	 */
	public void setId(String value) {
		myId = value;
	}
	
	/**
	 * Gets the media URL.
	 * 
	 * @return The value.
	 */
	@JsonProperty("media_url")
	public String getMediaUrl() {
		return myMediaUrl;
	}
	
	/**
	 * Sets the media URL.
	 * 
	 * @param value A value.
	 */
	public void setMediaUrl(String value) {
		myMediaUrl = value;
	}
	
	/**
	 * Gets the media URL.
	 * 
	 * @return The value.
	 */
	@JsonProperty("media_url_https")
	public String getMediaSecureUrl() {
		return myMediaSecureUrl;
	}
	
	/**
	 * Sets the media URL.
	 * 
	 * @param value A value.
	 */
	public void setMediaSecureUrl(String value) {
		myMediaSecureUrl = value;
	}
	
	/**
	 * Gets the URL.
	 * 
	 * @return The value.
	 */
	public String getUrl() {
		return myUrl;
	}
	
	/**
	 * Sets the URL.
	 * 
	 * @param value A value.
	 */
	public void setUrl(String value) {
		myUrl = value;
	}
	
	/**
	 * Gets the expanded URL.
	 * 
	 * @return The value.
	 */
	@JsonProperty("expanded_url")
	public String getExpandedUrl() {
		return myExpandedUrl;
	}
	
	/**
	 * Sets the expanded URL.
	 * 
	 * @param value A value.
	 */
	public void setExpandedUrl(String value) {
		myExpandedUrl = value;
	}
	
	/**
	 * Gets the display URL.
	 * 
	 * @return The value.
	 */
	@JsonProperty("display_url")
	public String getDisplayUrl() {
		return myDisplayUrl;
	}
	
	/**
	 * Sets the display URL.
	 * 
	 * @param value A value.
	 */
	public void setDisplayUrl(String value) {
		myDisplayUrl = value;
	}
	
	/**
	 * Gets the media type.
	 * 
	 * @return The value.
	 */
	@JsonProperty("type")
	public TwitterMediaType getMediaType() {
		return myMediaType;
	}
	
	/**
	 * Sets the media type.
	 * 
	 * @param value A value.
	 */
	public void setMediaType(TwitterMediaType value) {
		myMediaType = value;
	}
	
	/**
	 * Gets the sizes.
	 * 
	 * @return The collection.
	 */
	public Map<TwitterMediaSize, TwitterMediaSizeResponse> getSizes() {
		return mySizes;
	}
	
	/**
	 * Sets the sizes.
	 * 
	 * @param obj A collection.
	 */
	public void setSizes(Map<TwitterMediaSize, TwitterMediaSizeResponse> obj) {
		mySizes = obj;
	}
	
	/**
	 * Gets the video info.
	 * 
	 * @return The object. May be {@code null}.
	 */
	@JsonProperty("video_info")
	public TwitterVideoInfoResponse getVideoInfo() {
		return myVideoInfo;
	}
	
	/**
	 * Sets the video info.
	 * 
	 * @param obj An object. May be {@code null}.
	 */
	public void setVideoInfo(TwitterVideoInfoResponse obj) {
		myVideoInfo = obj;
	}
}
