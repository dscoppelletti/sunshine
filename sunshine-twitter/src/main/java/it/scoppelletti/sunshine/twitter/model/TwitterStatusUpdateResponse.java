/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

/**
 * Tweet object <abbr title="Also Known As">AKA</abbr> status update object.
 *
 * @see <a href="http://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object"
 *      target="_blank">Tweet Object</a>
 * @since 1.0.0
 */
@ToString(callSuper = true)
public class TwitterStatusUpdateResponse extends TwitterStatusDataResponse {
    private TwitterOriginalStatusResponse myQuotedStatus;
    private TwitterOriginalStatusResponse myRetweetedStatus;
    
	/**
	 * Sole constructor.
	 */
	public TwitterStatusUpdateResponse() {
	}
		
	/**
	 * Gets the representation of the original Tweet that was quoted.
	 * 
	 * @return The object. May be {@code null}.
	 */
	@JsonProperty("quoted_status")
	public TwitterOriginalStatusResponse getQuotedStatus() {
		return myQuotedStatus;
	}

	/**
	 * Sets the representation of the original Tweet that was quoted.
	 * 
	 * @param obj An object. May be {@code null}.
	 */
	public void setQuotedStatus(TwitterOriginalStatusResponse obj) {
		myQuotedStatus = obj;
	}
	
	/**
	 * Gets the representation of the original Tweet that was retweeted.
	 * 
	 * @return The object. May be {@code null}.
	 */
	@JsonProperty("retweeted_status")
	public TwitterOriginalStatusResponse getRetweetedStatus() {
		return myRetweetedStatus;
	}

	/**
	 * Sets the representation of the original Tweet that was retweeted.
	 * 
	 * @param obj An object. May be {@code null}.
	 */
	public void setRetweetedStatus(TwitterOriginalStatusResponse obj) {
		myRetweetedStatus = obj;
	}
}
