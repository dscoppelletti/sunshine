/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

/**
 * User mention that has been parsed out of the text of a Tweet.
 * 
 * @since 1.0.0
 */
@ToString(callSuper = true)
public class TwitterUserMentionResponse extends TwitterEntityResponse {
	private String myId;
	private String myName;
	private String myScreenName;
	
	/**
	 * Sole constructor.
	 */
	public TwitterUserMentionResponse() {
	}
	
	/**
	 * Gets the ID.
	 * 
	 * @return The value.
	 */
	@JsonProperty("id_str")
	public String getId() {
		return myId;
	}
	
	/**
	 * Sets the ID.
	 * 
	 * @param value A value.
	 */
	public void setId(String value) {
		myId = value;
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return The value.
	 */
	public String getName() {
		return myName;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param value A value.
	 */
	public void setName(String value) {
		myName = value;
	}
	
	/**
	 * Gets the screen name that this user identifies themselves with.
	 * 
	 * @return The value.
	 */
	@JsonProperty("screen_name")
	public String getScreenName() {
		return myScreenName;
	}
	
	/**
	 * Sets the screen name that this user identifies themselves with.
	 * 
	 * @param value A value.
	 */
	public void setScreenName(String value) {
		myScreenName = value;
	}
}
