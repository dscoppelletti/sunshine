/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.service;

import lombok.ToString;
import org.springframework.context.ApplicationEvent;
import it.scoppelletti.sunshine.twitter.model.TwitterStatusUpdateResponse;

/**
 * Event published when a status update has been delivered by a stream.
 * 
 * @see   it.scoppelletti.sunshine.twitter.TwitterStreamingService
 * @see   it.scoppelletti.sunshine.twitter.TwitterMessageHandler
 * @since 1.0.0
 */
@ToString(callSuper = true)
public final class TwitterStatusUpdateEvent extends ApplicationEvent {
	private static final long serialVersionUID = 1L;
	private final TwitterStatusUpdateResponse myStatusUpdate;
	
	/**
	 * Constructor.
	 * 
	 * @param source       The object on which the event initially occurred.
	 * @param statusUpdate The status update.
	 */
	public TwitterStatusUpdateEvent(Object source,
			TwitterStatusUpdateResponse statusUpdate) {
		super(source);
		
		if (statusUpdate == null) {
			throw new NullPointerException("Argument statusUpdate is null.");
		}
		
		myStatusUpdate = statusUpdate;
	}
	
	/**
	 * Gets the status update.
	 * 
	 * @return The object.
	 */
	public TwitterStatusUpdateResponse getStatusUpdate() {
		return myStatusUpdate;
	}
}
