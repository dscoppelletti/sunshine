/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import lombok.ToString;

/**
 * Video variant.
 * 
 * @since 1.0.0
 */
@ToString
public class TwitterVideoVariantResponse {
	private Integer myBitrate;
	private String myContentType;
	private String myUrl;
	
	/**
	 * Sole constructor.
	 */
	public TwitterVideoVariantResponse() {
	}
	
	/**
	 * Gets the bitrate.
	 * 
	 * @return The value. May be {@code null}.
	 */
	public Integer getBitrate() {
		return myBitrate;
	}
	
	/**
	 * Sets the bitrate.
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setBitrate(Integer value) {
		myBitrate = value;
	}
	
	/**
	 * Gets the content type.
	 * 
	 * @return The value.
	 */
	public String getContentType() {
		return myContentType;
	}
	
	/**
	 * Sets the content type.
	 * 
	 * @param value A value.
	 */
	public void setContentType(String value) {
		myContentType = value;
	}
	
	/**
	 * Gets the URL.
	 * 
	 * @return The value.
	 */
	public String getUrl() {
		return myUrl;
	}
	
	/**
	 * Sets the URL.
	 * 
	 * @param value A value.
	 */
	public void setUrl(String value) {
		myUrl = value;
	}
}
