/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import it.scoppelletti.sunshine.twitter.service.DefaultTwitterClientEventHandler;
import it.scoppelletti.sunshine.twitter.service.DefaultTwitterMessageHandler;
import it.scoppelletti.sunshine.twitter.service.DefaultTwitterStreamingService;
import it.scoppelletti.sunshine.twitter.service.TwitterClientEventHandler;
import it.scoppelletti.sunshine.twitter.service.TwitterMessageHandler;
import it.scoppelletti.sunshine.twitter.service.TwitterStreamingService;

/**
 * Configuration for Twitter.
 *
 * @since 1.0.0
 */
@Configuration
public class TwitterConfiguration {

	/**
	 * Sole constructor.
	 */
	public TwitterConfiguration() {
	}
	
	/**
	 * Defines the service to filter realtime Tweets.
	 * 
	 * @return The bean.
	 */
	@Bean
	@ConditionalOnMissingBean(TwitterStreamingService.class)
	public TwitterStreamingService twitterStreamingService() {
		return new DefaultTwitterStreamingService();
	}
	
	/**
	 * Defines the handler of the message queue for the filter realtime Tweets
	 * service.
	 * 
	 * @return The bean.
	 */
	@Bean
	@ConditionalOnMissingBean(TwitterMessageHandler.class)
	public TwitterMessageHandler twitterMessageHandler() {
		return new DefaultTwitterMessageHandler();
	}
	
	/**
	 * Defines the handler of the client event queue for the filter realtime
	 * Tweets service.
	 * 
	 * @return The bean.
	 */
	@Bean
	@ConditionalOnMissingBean(TwitterClientEventHandler.class)
	public TwitterClientEventHandler twitterClientEventHandler() {
		return new DefaultTwitterClientEventHandler();
	}
}
