/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

/**
 * Configuration properties for Twitter.
 * 
 * @since 1.0.0
 */
@Slf4j
@Validated
@ToString(exclude = "myAuth")
@ConfigurationProperties(prefix = TwitterProperties.PREFIX)
public class TwitterProperties implements InitializingBean {
	
	/**
	 * The name prefix of the properties.
	 */
	public static final String PREFIX = "it.scoppelletti.twitter";

	private TwitterProperties.AuthProperties myAuth;
	private TwitterProperties.StreamingProperties myHbc;
	
	/**
	 * Sole constructors.
	 */
	public TwitterProperties() {
		myHbc = new TwitterProperties.StreamingProperties();
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		myLogger.info(toString());
	}
	
	/**
	 * Gets the authentication properties.
	 * 
	 * @return The object.
	 */
	@Valid
	@NotNull
	public TwitterProperties.AuthProperties getAuth() {
		return myAuth;
	}
	
	/**
	 * Sets the authentication properties.
	 * 
	 * @param obj An object.
	 */
	public void setAuth(TwitterProperties.AuthProperties obj) {
		myAuth = obj;
	}
	
	/**
	 * Gets the streaming properties.
	 * 
	 * @return The object.
	 */
	public TwitterProperties.StreamingProperties getHbc() {
		return myHbc;
	}
	
	/**
	 * Authentication properties.
	 * 
	 * @since 1.0.0
	 */
	public static class AuthProperties {
		private String myConsumerKey;
		private String myConsumerSecret;
		private String myAccessToken;
		private String myTokenSecret;
		
		/**
		 * Sole constructor.
		 */
		public AuthProperties() {
		}
		
		/**
		 * Gets the consumer key.
		 * 
		 * @return The value.
		 */
		@NotBlank
		public String getConsumerKey() {
			return myConsumerKey;
		}
		
		/**
		 * Sets the consumer key.
		 * 
		 * @param value A value.
		 */
		public void setConsumerKey(String value) {
			myConsumerKey = value;
		}
		
		/**
		 * Gets the consumer secret.
		 * 
		 * @return The value.
		 */
		@NotBlank
		public String getConsumerSecret() {
			return myConsumerSecret;
		}
		
		/**
		 * Sets the consumer secret.
		 * 
		 * @param value A value.
		 */
		public void setConsumerSecret(String value) {
			myConsumerSecret = value;
		}
		
		/**
		 * Gets the access token.
		 * 
		 * @return The value.
		 */
		@NotBlank
		public String getAccessToken() {
			return myAccessToken;
		}
		
		/**
		 * Sets the access token.
		 * 
		 * @param value A value.
		 */
		public void setAccessToken(String value) {
			myAccessToken = value;
		}
		
		/**
		 * Gets the token secrets.
		 * 
		 * @return The value.
		 */
		@NotBlank
		public String getTokenSecret() {
			return myTokenSecret;
		}
		
		/**
		 * Sets the token secret.
		 * 
		 * @param value A value.
		 */
		public void setTokenSecret(String value) {
			myTokenSecret = value;
		}
	}
	
	/**
	 * Configuration properties for filtering realtime Tweets.
	 * 
	 * @see   it.scoppelletti.sunshine.twitter.TwitterStreamingService
	 * @see   <a href="http://github.com/twitter/hbc" target="_blank">Hosebird
	 *        Client (hbc)</a>
	 * @since 1.0.0
	 */
	@ToString
	public static class StreamingProperties {
		private int myMessageQueueSize;
		private int myEventQueueSize;
		private int myBackfillCount;
		
		/**
		 * Sole constructor.
		 */
		public StreamingProperties() {
			myMessageQueueSize = 100000;
			myEventQueueSize = 1000;
			myBackfillCount = 1000;
		}
		
		/**
		 * Gets the message queue size.
		 * 
		 * @return The value.
		 */
		public int getMessageQueueSize() {
			return myMessageQueueSize;
		}
		
		/**
		 * Sets the message queue size.
		 * 
		 * <p>Be sure to size this properly based on expected TPS of your
		 * stream.</p>
		 * 
		 * @param value A value.
		 */
		public void setMessageQueueSize(int value) {
			myMessageQueueSize = value;
		}
		
		/**
		 * Gets the event queue size.
		 * 
		 * @return The value.
		 */
		public int getEventQueueSize() {
			return myEventQueueSize;
		}
		
		/**
		 * Sets the event queue size.
		 * 
		 * <p>Be sure to size this properly based on expected TPS of your
		 * stream.</p>
		 * 
		 * @param value A value.
		 */
		public void setEventQueueSize(int value) {
			myEventQueueSize = value;
		}
	}
}
