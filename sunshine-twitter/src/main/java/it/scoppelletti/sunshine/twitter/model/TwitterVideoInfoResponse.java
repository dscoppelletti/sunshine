/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

/**
 * Video info.
 * 
 * @since 1.0.0
 */
@ToString
public class TwitterVideoInfoResponse {
	private int[] myAspectRatio;
	private long myDuration;
	private List<TwitterVideoVariantResponse> myVariants;
	
	/**
	 * Sole constructor.
	 */
	public TwitterVideoInfoResponse() {
	}
	
	/**
	 * Gets the aspect ratio.
	 * 
	 * @return The array.
	 */
	@JsonProperty("aspect_ratio")
	public int[] getAspectRatio() {
		return myAspectRatio;
	}
	
	/**
	 * Sets the aspect ratio.
	 * 
	 * @param v An array.
	 */
	public void setAspectRatio(int[] v) {
		myAspectRatio = v;
	}
	
	/**
	 * Gets the duration.
	 * 
	 * @return The value (milliseconds).
	 */
	@JsonProperty("duration_millis")
	public long getDuration() {
		return myDuration;
	}
	
	/**
	 * Sets the duration.
	 * 
	 * @param value The value (milliseconds).
	 */
	public void setDuration(long value) {
		myDuration = value;
	}
	
	/**
	 * Gets the variants.
	 * 
	 * @return The collection.
	 */
	public List<TwitterVideoVariantResponse> getVariants() {
		return myVariants;
	}
	
	/**
	 * Sets the variants.
	 * 
	 * @param obj A collection.
	 */
	public void setVariants(List<TwitterVideoVariantResponse> obj) {
		myVariants = obj;
	}
}
