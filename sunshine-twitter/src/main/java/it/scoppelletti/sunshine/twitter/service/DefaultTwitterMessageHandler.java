/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.service;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import it.scoppelletti.sunshine.twitter.model.TwitterStatusUpdateResponse;

/**
 * Default implementation of the {@code TwitterMessageHandler} interface.
 * 
 * @since 1.0.0
 */
@Slf4j
public class DefaultTwitterMessageHandler implements TwitterMessageHandler {
	
	@Autowired
	private ObjectMapper myObjectMapper;
	
	@Autowired
	private ApplicationEventPublisher myEventPublisher;
	
	/**
	 * Sole constructor.
	 */
	public DefaultTwitterMessageHandler() {
	}
	
	@Async
	@Override
	public void run(BlockingQueue<String> queue) {
		String item;
		TwitterStatusUpdateResponse msg;
		TwitterStatusUpdateEvent event;
		
		if (queue == null) {
			throw new NullPointerException("Argument queue is null.");
		}
		
		while (true) {
        		try {
        			item = queue.take();
        		} catch (InterruptedException ex) {
        			myLogger.error(ex.getMessage(), ex);
        			break;
        		}		
        		
        		if (TwitterMessageHandler.MESSAGE_STOP.equals(item)) {
        			myLogger.info("Received stop message.");
        			break;
        		}
        		
        		try {
        			msg = myObjectMapper.readValue(item,
        					TwitterStatusUpdateResponse.class);
        		} catch (IOException ex) {
        			myLogger.error(String.format("Failed to parse: %1$s.", item),
        					ex);
        			continue;
        		}	
        		
        		myLogger.debug("tweet={}", msg);
        		event = new TwitterStatusUpdateEvent(this, msg);
        		
        		try {
        			myEventPublisher.publishEvent(event);
        		} catch (RuntimeException ex) {
        			myLogger.error(String.format("Failed to handle event: %1$s.",
        					event), ex);
        		}
		}
	}
}
