/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import java.time.LocalDateTime;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.ToString;
import it.scoppelletti.sunshine.twitter.i18n.LocalDateTimeDeserializer;

/**
 * Poll in a Tweet.
 * 
 * @see <a href="http://developer.twitter.com/en/docs/tweets/enrichments/overview/poll-metadata"
 *      target="_blank">Poll metadata</a>
 * @since 1.0.0
 */
@ToString
public class TwitterPollResponse {
	private List<TwitterPollOptionResponse> myOptions;
	private LocalDateTime myEndDateTime;
	private int myDuration;
	
	/*
	 * Sole constructor.
	 */
	public TwitterPollResponse() {
	}
	
	/**
	 * Gets the options.
	 * 
	 * @return The collection (between two and four items).
	 */
	public List<TwitterPollOptionResponse> getOptions() {
		return myOptions;
	}
	
	/**
	 * Sets the options.
	 * 
	 * @param obj A collection (between two and four items).
	 */
	public void setOptions(List<TwitterPollOptionResponse> obj) {
		myOptions = obj;
	}
	
	/**
	 * Gets the expiration time.
	 * 
	 * @return The value.
	 */
	@JsonProperty("end_datetime")
	public LocalDateTime getEndDateTime() {
		return myEndDateTime;
	}
	
	/**
	 * Sets the expiration time.
	 * 
	 * @param value A value.
	 */
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	public void setEndDateTime(LocalDateTime value) {
		myEndDateTime = value;
	}
	
	/**
	 * Gets the duration.
	 * 
	 * @return A value (minutes).
	 */
	@JsonProperty("duration_minutes")
	public int getDuration() {
		return myDuration;
	}
	
	/**
	 * Sets the duration.
	 * 
	 * @param value A value (minutes).
	 */
	public void setDuration(int value) {
		myDuration = value;
	}
}
