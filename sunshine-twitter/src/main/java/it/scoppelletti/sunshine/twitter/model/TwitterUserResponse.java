/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.ToString;
import it.scoppelletti.sunshine.twitter.TwitterWithheldScope;
import it.scoppelletti.sunshine.twitter.i18n.LocalDateTimeDeserializer;

/**
 * User object.
 * 
 * @see <a href="http://developer.twitter.com/en/docs/tweets/data-dictionary/overview/user-object"
 *      target="_blank">User Object"</a>
 * @since 1.0.0
 */
@ToString
public class TwitterUserResponse {
	private String myId;
	private String myName;
	private String myScreenName;
	private String myLocation;
	private String myUrl;
	private String myDesc;
	private boolean myProtected;
	private boolean myVerified;
	private int myFollowersCount;
	private int myFriendsCount;
	private int myListedCount;
	private int myFavoritesCount;
	private int myStatusesCount;
	private LocalDateTime myCreatedAt;
	private Integer myUtcOffset;
	private String myTimeZone;
	private boolean myGeoEnabled;
	private String myLang;
	private boolean myContributorsEnabled;
	private String myProfileBackgroundColor;
	private String myProfileBackgroundUrl;
	private String myProfileBackgroundSecureUrl;
	private Boolean myProfileBackgroundTile;
	private String myProfileBannerUrl;
	private String myProfileImageUrl;
	private String myProfileImageSecureUrl;
	private String myProfileLinkColor;
	private String myProfileSidebarBorderColor;
	private String myProfileSidebarFillColor;
	private String myProfileTextColor;
	private boolean myProfileUseBackgroundImage;
	private boolean myDefaultProfile;
	private boolean myDefaultProfileImage;
	private String myWithheldInCountries;
	private TwitterWithheldScope myWithheldScope;
	
	/**
	 * Sole constructor.
	 */
	public TwitterUserResponse() {
	}
	
	/**
	 * Gets the ID.
	 * 
	 * @return The value.
	 */
	@JsonProperty("id_str")
	public String getId() {
		return myId;
	}
	
	/**
	 * Sets the ID.
	 * 
	 * @param value A value.
	 */
	public void setId(String value) {
		myId = value;
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return The value.
	 */
	public String getName() {
		return myName;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param value A value.
	 */
	public void setName(String value) {
		myName = value;
	}
	
	/**
	 * Gets the screen name that this user identifies themselves with.
	 * 
	 * @return The value.
	 */
	@JsonProperty("screen_name")
	public String getScreenName() {
		return myScreenName;
	}
	
	/**
	 * Sets the screen name that this user identifies themselves with.
	 * 
	 * @param value A value.
	 */
	public void setScreenName(String value) {
		myScreenName = value;
	}

	/**
	 * Gets the user-defined location for this account's profile.
	 * 
	 * @return The value. May be {@code null}.
	 */
	public String getLocation() {
		return myLocation;
	}
	
	/**
	 * Sets the user-defined location for this account's profile.
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setLocation(String value) {
		value = myLocation;
	}
	
	/**
	 * Gets the URL provided by the user in association with their profile.
	 * 
	 * @return The value. May be {@code null}.
	 */
	public String getUrl() {
		return myUrl;
	}
	
	/**
	 * Sets the URL provided by the user in association with their profile.
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setUrl(String value) {
		myUrl = value;
	}
	
	/**
	 * Gets the description.
	 * 
	 * @return The value. May be {@code null}.
	 */
	public String getDescription() {
		return myDesc;
	}
	
	/**
	 * Sets the description.
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setDescription(String value) {
		myDesc = value;
	}
	
	/**
	 * Indicates whether this user has chosen to protect their Tweets.
	 * 
	 * @return Returns {@code true} if this user has chosen to protect their
	 *         Tweets, {@code false} otherwise.
	 * @see <a href="http://support.twitter.com/articles/14016"
	 *      target="_blank">About public and protected Tweets</a>
	 */
	public boolean isProtected() {
		return myProtected;
	}
	
	/**
	 * Sets whether this user has chosen to protect their Tweets.
	 * 
	 * @param value Whether this user has chosen to protect their Tweets.
	 */
	public void setProtected(boolean value) {
		myProtected = value;
	}
	
	/**
	 * Indicates whether this user has a verified account.
	 * 
	 * @return Returns {@code true} if this user has a verified account,
	 *         {@code false} otherwise.
	 * @see <a href="http://support.twitter.com/articles/119135"
	 *         target="_blank">About verified accounts</a>
	 */
	public boolean isVerified() {
		return myVerified;
	}
	
	/**
	 * Sets whether this user has a verified account.
	 * 
	 * @param value Whether this user has a verified account.
	 */
	public void setVerified(boolean value) {
		myVerified = value;
	}
	
	/**
	 * Gets the number of followers this account currently has.
	 * 
	 * @return The value.
	 */
	@JsonProperty("followers_count")
	public int getFollowersCount() {
		return myFollowersCount;
	}
	
	/**
	 * Sets the number of followers this account currently has.
	 * 
	 * @param value A value.
	 */
	public void setFollowersCount(int value) {
		myFollowersCount = value;
	}
	
	/**
	 * Gets the number of users this account is following (AKA their
	 * <dfn>followings</dfn>).
	 * 
	 * @return The value.
	 */
	@JsonProperty("friends_count")
	public int getFriendsCount() {
		return myFriendsCount;
	}
	
	/**
	 * Sets the number of user this account is following.
	 * 
	 * @param value A value.
	 */
	public void setFriendsCount(int value) {
		myFriendsCount = value;
	}
	
	/**
	 * Gets the number of public lists that this user is a member of.
	 * 
	 * @return The value.
	 */
	@JsonProperty("listed_count")
	public int getListedCount() {
		return myListedCount;
	}
	
	/**
	 * Sets the number of public lists that this user is a member of.
	 * 
	 * @param value A value.
	 */
	public void setListedCount(int value) {
		myListedCount = value;
	}
	
	/**
	 * Gets the number of Tweets this user has liked in this account's lifetime.
	 * 
	 * @return The value.
	 */
	@JsonProperty("favourites_count")
	public int getFavoritesCount() {
		return myFavoritesCount;
	}
	
	/**
	 * Sets the number of Tweets this user has liked in this account's lifetime.
	 * 
	 * @param value A value.
	 */
	public void setFavoritesCount(int value) {
		myFavoritesCount = value;
	}
	
	/**
	 * Gets the number of Tweets (including retweets) issued by this user.
	 * 
	 * @return The value.
	 */
	@JsonProperty("statuses_count")
	public int getStatusesCount() {
		return myStatusesCount;
	}
	
	/**
	 * Sets the number of Tweets (including retweets) issued by this user.
	 * 
	 * @param value A value.
	 */
	public void setStatusesCount(int value) {
		myStatusesCount = value;
	}
	
	/**
	 * Gets the UTC datetime that the user account was created on Twitter.
	 * 
	 * @return The value.
	 */
	@JsonProperty("created_at")
	public LocalDateTime getCreatedAt() {
		return myCreatedAt;
	}
	
	/**
	 * Sets the UTC datetime that the user account was created on Twitter.
	 * 
	 * @param value A value.
	 */
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	public void setCreatedAt(LocalDateTime value) {
		myCreatedAt = value;
	}
	
	/**
	 * Gets the offset from GMT/UTC.
	 * 
	 * @return The value (seconds).
	 */
	@JsonProperty("utc_offset")
	public Integer getUtcOffset() {
		return myUtcOffset;
	}
	
	/**
	 * Sets the offset from GMT/UTC.
	 * 
	 * @param value A value (seconds).
	 */
	public void setUtcOffset(Integer value) {
		myUtcOffset = value;
	}
	
	/**
	 * Gets the string describing the Time Zone this user declares themselves
	 * within.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("time_zone")
	public String getTimeZone() {
		return myTimeZone;
	}
	
	/**
	 * Sets the string describing the Time Zone this user declares themselves
	 * within.
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setTimeZone(String value) {
		myTimeZone = value;
	}
	
	/**
	 * Indicates whether this user has enabled the possibility of geotagging
	 * their Tweets.
	 * 
	 * @return Returns {@code true} if this user has enabled the possibility of
	 *         geotagging their Tweets, {@code false} otherwise.
	 */
	@JsonProperty("geo_enabled")
	public boolean isGeoEnabled() {
		return myGeoEnabled;
	}
	
	/**
	 * Sets whether this user has enabled the possibility of geotagging their
	 * Tweets.
	 * 
	 * @param value Whether this user has enabled the possibility of geotagging
	 *              their Tweets.
	 */
	public void setGeoEnabled(boolean value) {
		myGeoEnabled = value;
	}
	
	/**
	 * Gets user's self-declared user interface language.
	 * 
	 * @return The value (BCP 47 code).
	 */
	public String getLang() {
		return myLang;
	}
	
	/**
	 * Sets the user's self-declared user interface language.
	 * 
	 * @param value A value (BCP 47 code).
	 */
	public void setLang(String value) {
		myLang = value;
	}
	
	/**
	 * Indicates whether this user has an account with <dfn>contributor
	 * mode</dfn> enabled, allowing for Tweets issued by this user to be
	 * co-authored by another account.
	 *
	 * @return Returns {@code true} if this user has an account with contributor
	 *         mode enabled, {@code false} otherwise.
	 */
	@JsonProperty("contributors_enabled")
	public boolean isContributorsEnabled() {
		return myContributorsEnabled;
	}
	
	/**
	 * Sets whether this user has an account with contributor mode enabled.
	 * 
	 * @param value Whether this user has an account with contributor mode
	 *              enabled.
	 */
	public void setContributorsEnabled(boolean value) {
		myContributorsEnabled = value;
	}
	
	/**
	 * Gets the color chosen by this user for their background.
	 * 
	 * @return The value (RGB color). May be {@code null}.
	 */
	@JsonProperty("profile_background_color")
	public String getProfileBackgroundColor() {
		return myProfileBackgroundColor;
	}
	
	/**
	 * Sets the color chosen by this user for their background.
	 * 
	 * @param value A value (RGB color). May be {@code null}.
	 */
	public void setProfileBackgroundColor(String value) {
		myProfileBackgroundColor = value;
	}
	
	/**
	 * Gets the URL of the background image this user has uploaded for their
	 * profile.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("profile_background_image_url")
	public String getProfileBackgroundUrl() {
		return myProfileBackgroundUrl;
	}
	
	/**
	 * Sets the URL of the background image this user has uploaded for their
	 * profile.
	 * 
	 * @return A value. May be {@code null}.
	 */
	public void setProfileBackgroundUrl(String value) {
		myProfileBackgroundUrl = value;
	}
	
	/**
	 * Gets the URL of the background image this user has uploaded for their
	 * profile.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("profile_background_image_url_https")
	public String getProfileBackgroundSecureUrl() {
		return myProfileBackgroundSecureUrl;
	}
	
	/**
	 * Sets the URL of the background image this user has uploaded for their
	 * profile.
	 * 
	 * @return A value. May be {@code null}.
	 */
	public void setProfileBackgroundSecureUrl(String value) {
		myProfileBackgroundSecureUrl = value;
	}
	
	/**
	 * Indicates whether this user's background image should be tiled when
	 * displayed. 
	 * 
	 * @return Returns {@code true} if this user's background image should be
	 *         tiled when displayed, {@code false} otherwise.
	 */
	@JsonProperty("profile_background_tile")
	public boolean isProfileBackgroundTile() {
		return myContributorsEnabled;
	}
	
	/**
	 * Sets whether this user's background image should be tiled when displayed. 
	 * 
	 * @param value Whether this user's background image should be tiled when
	 *              displayed. 
	 */
	public void setProfileBackgroundTile(boolean value) {
		myContributorsEnabled = value;
	}
	
	/**
	 * Gets the URL of the standard web representation of this user's uploaded
	 * profile banner.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("profile_banner_url")
	public String getProfileBannerUrl() {
		return myProfileBannerUrl;
	}
	
	/**
	 * Sets the URL of the standard web representation of this user's uploaded
	 * profile banner.
	 * 
	 * @return A value. May be {@code null}.
	 */
	public void setProfileBannerUrl(String value) {
		myProfileBannerUrl = value;
	}
	
	/**
	 * Gets the URL of this user's profile image.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("profile_image_url")
	public String getProfileImageUrl() {
		return myProfileImageUrl;
	}
	
	/**
	 * Sets the URL of this user's profile image.
	 * 
	 * @return A value. May be {@code null}.
	 */
	public void setProfileImageUrl(String value) {
		myProfileImageUrl = value;
	}
	
	/**
	 * Gets the URL of this user's profile image.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("profile_image_url_https")
	public String getProfileImageSecureUrl() {
		return myProfileImageSecureUrl;
	}
	
	/**
	 * Sets the URL of this user's profile image.
	 * 
	 * @return A value. May be {@code null}.
	 */
	public void setProfileImageSecureUrl(String value) {
		myProfileImageSecureUrl = value;
	}
	
	/**
	 * Gets the color chosen by this user to display links with in their Twitter
	 * UI.
	 * 
	 * @return The value (RGB color). May be {@code null}.
	 */
	@JsonProperty("profile_link_color")
	public String getProfileLinkColor() {
		return myProfileLinkColor;
	}
	
	/**
	 * Sets the color chosen by this user to display links with in their Twitter
	 * UI.
	 * 
	 * @param value A value (RGB color). May be {@code null}.
	 */
	public void setProfileLinkColor(String value) {
		myProfileLinkColor = value;
	}
	
	/**
	 * Gets the color chosen by this user to display sidebar borders with in
	 * their Twitter UI.
	 * 
	 * @return The value (RGB color). May be {@code null}.
	 */
	@JsonProperty("profile_sidebar_border_color")
	public String getProfileSidebarBorderColor() {
		return myProfileSidebarBorderColor;
	}
	
	/**
	 * Sets the color chosen by this user to display sidebar borders with in
	 * their Twitter UI.
	 * 
	 * @param value A value (RGB color). May be {@code null}.
	 */
	public void setProfileSidebarBorderColor(String value) {
		myProfileSidebarBorderColor = value;
	}
	
	/**
	 * Gets the color chosen by this user to display sidebar backgrounds with in
	 * their Twitter UI.
	 * 
	 * @return The value (RGB color). May be {@code null}.
	 */
	@JsonProperty("profile_sidebar_fill_color")
	public String getProfileSidebarFillColor() {
		return myProfileSidebarFillColor;
	}
	
	/**
	 * Sets the color chosen by this user to display sidebar backgrounds with in
	 * their Twitter UI.
	 * 
	 * @param value A value (RGB color). May be {@code null}.
	 */
	public void setProfileSidebarFillColor(String value) {
		myProfileSidebarFillColor = value;
	}
	
	/**
	 * Gets the color chosen by this user to display text with in their Twitter
	 * UI.
	 * 
	 * @return The value (RGB color). May be {@code null}.
	 */
	@JsonProperty("profile_text_color")
	public String getProfileTextColor() {
		return myProfileTextColor;
	}
	
	/**
	 * Sets the color chosen by this user to display text with in their Twitter
	 * UI.
	 * 
	 * @param value A value (RGB color). May be {@code null}.
	 */
	public void setProfileTextColor(String value) {
		myProfileTextColor = value;
	}
	
	/**
	 * Indicates whether this user wants their uploaded background image to be
	 * used.
	 * 
	 * @return Returns {@code true} if this user wants their uploaded background
	 *         image to be used, {@code false} otherwise.
	 */
	@JsonProperty("profile_use_background_image")
	public boolean isProfileUseBackgroundImage() {
		return myProfileUseBackgroundImage;
	}
	
	/**
	 * Sets whether this user wants their uploaded background image to be used.
	 * 
	 * @param value Whether this user wants their uploaded background image to
	 *              be used.
	 */
	public void setProfileUseBackgroundImage(boolean value) {
		myProfileUseBackgroundImage = value;
	}
	
	/**
	 * Indicates whether this user has not altered the theme or background of
	 * their profile.
	 * 
	 * @return Returns {@code true} if this user has not altered the theme or
	 *         background of their profile, {@code false} otherwise.
	 */
	@JsonProperty("default_profile")
	public boolean isDefaultProfile() {
		return myDefaultProfile;
	}
	
	/**
	 * Sets whether this user has not altered the theme or background of their
	 * profile.
	 * 
	 * @param value Whether this user has not altered the theme or background of
	 *              their profile.
	 */
	public void setDefaultProfile(boolean value) {
		myDefaultProfile = value;
	}
	
	/**
	 * Indicates whether this user has not uploaded their own profile image and
	 * a default image is used instead. 
	 * 
	 * @return Returns {@code true} if this user has not not uploaded their own
	 *         profile image, {@code false} otherwise.
	 */
	@JsonProperty("default_profile_image")
	public boolean isDefaultProfileImage() {
		return myDefaultProfileImage;
	}
	
	/**
	 * Sets whether this user has not uploaded their own profile image and a
	 * default image is used instead. 
	 * 
	 * @param value Whether this user has not uploaded their own profile image.
	 */
	public void setDefaultProfileImage(boolean value) {
		myDefaultProfileImage = value;
	}
	
	/**
	 * Gets the countries this user is withheld from.
	 * 
	 * @return The value (two-letter country codes separated by commas). May be
	 *         {@code null}.
	 */
	@JsonProperty("withheld_in_countries")
	public String getWithheldInCountries() {
		return myWithheldInCountries;
	}
	
	/**
	 * Sets the countries this user is withheld from.
	 * 
	 * @param value A value (two-letter country codes separated by commas). May
	 *        be {@code null}.
	 */
	public void setWithheldInCountries(String value) {
		myWithheldInCountries = value;
	}
	
	/**
	 * Gets the content being withheld.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("withheld_scope")
	public TwitterWithheldScope getWithheldScope() {
		return myWithheldScope;
	}
	
	/**
	 * Sets the content being withheld.
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setWithheldScope(TwitterWithheldScope value) {
		myWithheldScope = value;
	}
}
