/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.scoppelletti.sunshine.twitter.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;
import it.scoppelletti.sunshine.twitter.TwitterResizeMode;

/**
 * Media size.
 *
 * @since 1.0.0
 */
@ToString
public class TwitterMediaSizeResponse {
	private int myWidth;
	private int myHeight;
	private TwitterResizeMode myResizeMode;
	
	/**
	 * Sole constructor.
	 */
	public TwitterMediaSizeResponse() {
	}
	
	/**
	 * Gets the width.
	 * 
	 * @return The value (pixels).
	 */
	@JsonProperty("w")
	public int getWidth() {
		return myWidth;
	}
	
	/**
	 * Sets the width.
	 * 
	 * @param value A value (pixels).
	 */
	public void setWidth(int value) {
		myWidth = value;
	}
	
	/**
	 * Gets the height.
	 * 
	 * @return The value (pixels).
	 */
	@JsonProperty("h")
	public int getHeight() {
		return myHeight;
	}
	
	/**
	 * Sets the height.
	 * 
	 * @param value A value (pixels).
	 */
	public void setHeight(int value) {
		myHeight = value;
	}
	
	/**
	 * Gets the resize mode.
	 * 
	 * @return The value.
	 */
	@JsonProperty("resize")
	public TwitterResizeMode getResizeMode() {
		return myResizeMode;
	}
	
	/**
	 * Sets the resize mode.
	 * 
	 * @param value A value.
	 */
	public void setResizeMode(TwitterResizeMode value) {
		myResizeMode = value;
	}
}
