/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

/**
 * Entities which have been parsed out of the text of a Tweet.
 * 
 * @since 1.0.0
 */
@ToString
public class TwitterEntitiesResponse {
	private List<TwitterHashtagResponse> myHashtags;
	private List<TwitterUrlResponse> myUrls;
	private List<TwitterUserMentionResponse> myUserMentions;
	private List<TwitterMediaResponse> myMedia;
	private List<TwitterSymbolResponse> mySymbols;
	private List<TwitterPollResponse> myPolls;
	
	/**
	 * Sole constructor.
	 */
	public TwitterEntitiesResponse() {
	}
	
	/**
	 * Gets the hashtags.
	 * 
	 * @return The collection. May be {@code null}.
	 */
	public List<TwitterHashtagResponse> getHashtags() {
		return myHashtags;
	}
	
	/**
	 * Sets the hashtags.
	 * 
	 * @param obj A collection. May be {@code null}.
	 */
	public void setHashtags(List<TwitterHashtagResponse> obj) {
		myHashtags = obj;
	}
	
	/**
	 * Gets the URLs.
	 * 
	 * @return The collection. May be {@code null}.
	 */
	public List<TwitterUrlResponse> getUrls() {
		return myUrls;
	}
	
	/**
	 * Sets the URLs.
	 * 
	 * @param obj A collection. May be {@code null}.
	 */
	public void setUrls(List<TwitterUrlResponse> obj) {
		myUrls = obj;
	}
	
	/**
	 * Gets the user mentions.
	 * 
	 * @return The collection. May be {@code null}.
	 */
	@JsonProperty("user_mentions")
	public List<TwitterUserMentionResponse> getUserMentions() {
		return myUserMentions;
	}
	
	/**
	 * Sets the user mentions.
	 * 
	 * @param obj A collection. May be {@code null}.
	 */
	public void setUserMentions(List<TwitterUserMentionResponse> obj) {
		myUserMentions = obj;
	}
	
	/**
	 * Gets the media.
	 * 
	 * @return The collection. May be {@code null}.
	 */
	public List<TwitterMediaResponse> getMedia() {
		return myMedia;
	}
	
	/**
	 * Sets the media.
	 * 
	 * @param obj A collection. May be {@code null}.
	 */
	public void setMedia(List<TwitterMediaResponse> obj) {
		myMedia = obj;
	}
	
	/**
	 * Gets the symbols.
	 * 
	 * @return The collection. May be {@code null}.
	 */
	public List<TwitterSymbolResponse> getSymbols() {
		return mySymbols;
	}
	
	/**
	 * Sets the symbols.
	 * 
	 * @param obj A collection. May be {@code null}.
	 */
	public void setSymbols(List<TwitterSymbolResponse> obj) {
		mySymbols = obj;
	}
	
	/**
	 * Gets the polls.
	 * 
	 * @return The collection. May be {@code null}.
	 */
	public List<TwitterPollResponse> getPolls() {
		return myPolls;
	}
	
	/**
	 * Sets the polls.
	 * 
	 * @param obj A collection. May be {@code null}.
	 */
	public void setPolls(List<TwitterPollResponse> obj) {
		myPolls = obj;
	}
}
