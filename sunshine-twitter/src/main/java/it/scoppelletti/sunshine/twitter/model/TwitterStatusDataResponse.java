/*
 * Copyright (C) 2017 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.twitter.model;

import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.ToString;
import it.scoppelletti.sunshine.twitter.TwitterFilterLevel;
import it.scoppelletti.sunshine.twitter.i18n.LocalDateTimeDeserializer;

/**
 * Tweet object <abbr title="Also Known As">AKA</abbr> status update object.
 *
 * @since 1.0.0
 */
@ToString
public abstract class TwitterStatusDataResponse {
	// In order to support coordinates and places properties I should use an
	// implementation of geoJson and a third-party Jackson module for
	// deserialization.
	private LocalDateTime myCreatedAt;
	private String myId;
	private String myText;
	private String mySource;
	private boolean myTruncated;
	private String myInReplyToStatusId;
	private String myInReplyToUserId;
	private String myInReplyToScreenName;
	private TwitterUserResponse myUser;
	private String myQuotedStatusId;
    private boolean myQuoteStatus;
    private Integer myQuoteCount;
    private int myReplyCount;
    private int myRetweetCount;
    private Integer myFavoriteCount;
    private TwitterEntitiesResponse myEntities;
    private TwitterExtendedEntitiesResponse myExtEntities;
    private Boolean myFavorited;
    private Boolean myRetweeted;
    private Boolean myPossiblySensitive;
    private TwitterFilterLevel myFilterLevel;
    private String myLang;
    
	/**
	 * Sole constructor.
	 */
	protected TwitterStatusDataResponse() {
	}
	
	/**
	 * Gets the UTC time when this Tweet was created.
	 * 
	 * @return The value.
	 */
	@JsonProperty("created_at")
	public LocalDateTime getCreatedAt() {
		return myCreatedAt;
	}
	
	/**
	 * Sets the UTC time when this Tweet was created.
	 * 
	 * @param value A value.
	 */
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	public void setCreatedAt(LocalDateTime value) {
		myCreatedAt = value;
	}
	
	/**
	 * Gets the ID.
	 * 
	 * @return The value.
	 */
	@JsonProperty("id_str")
	public String getId() {
		return myId;
	}
	
	/**
	 * Sets the ID.
	 * 
	 * @param value A value.
	 */
	public void setId(String value) {
		myId = value;
	}
		
	/**
	 * Gets the text.
	 *  
	 * @return The value.
	 * @see    #isTruncated
	 */
	public String getText() {
		return myText;
	}
	
	/**
	 * Sets the text.
	 * 
	 * @param value A value.
	 */
	public void setText(String value) {
		myText = value;
	}
	
	/**
	 * Gets the utility used to post this Tweet.
	 * 
	 * @return The value.
	 */
	public String getSource() {
		return mySource;
	}
	
	/**
	 * Sets the utility used to post this Tweet.
	 * 
	 * @param value A value.
	 */
	public void setSource(String value) {
		mySource = value;
	}
	
	/**
	 * Indicates whether the value of the text was truncated.
	 * 
	 * <p>Truncated text will end in ellipsis, like this {@code ...}. Since
	 * Twitter now rejects long Tweets vs truncating them, the large majority of
	 * Tweets will have this set to {@code false}.<br>
	 * However, {@code text} could be truncated, for example, as a result of a
	 * retweet exceeding the original Tweet text length limit of 140 characters.
	 * Note that while native retweets may have their toplevel {@code text}
	 * property shortened, the original text will be available under the 
	 * {@code retweeted_status} object and the {@code truncated} property will
	 * be set to the value of the original status (in most cases,
	 * {@code false}).</p>
	 * 
	 * @return Returns {@code true} if the {@code text} property was truncated,
	 *         {@code false} otherwise.
	 * @see    #getText
	 */
	public boolean isTruncated() {
		return myTruncated;
	}
	
	/**
	 * Sets whether the value of the text was truncated.
	 * 
	 * @param value Whether the value of the text was truncated.
	 */
	public void setTruncated(boolean value) {
		myTruncated = value;
	}
	
	/**
	 * If this Tweet is a reply, gets the original Tweet's ID.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("in_reply_to_status_id_str")
	public String getInReplyToStatusId() {
		return myInReplyToStatusId;
	}
	
	/**
	 * If this Tweet is a reply, gets the original Tweet's ID.
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setInReplyToStatusId(String value) {
		myInReplyToStatusId = value;
	}
		
	/**
	 * If this Tweet is a reply, gets the original Tweet's author ID.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("in_reply_to_user_id_str")
	public String getInReplyToUserId() {
		return myInReplyToUserId;
	}
	
	/**
	 * If this Tweet is a reply, sets the original Tweet's author ID.
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setInReplyToUserId(String value) {
		myInReplyToUserId = value;
	}
	
	/**
	 * If this Tweet is a reply, gets the screen name of the original Tweet's
	 * author.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("in_reply_to_screen_name")
	public String getInReplyToScreenName() {
		return myInReplyToScreenName;
	}
	
	/**
	 * If this Tweet is a reply, sets the screen name of the original Tweet's
	 * author.
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setInReplyToScreenName(String value) {
		myInReplyToScreenName = value;
	}
	
	/**
	 * Gets the user who posted this Tweet.
	 * 
	 * @return The object.
	 */
	public TwitterUserResponse getUser() {
		return myUser;
	}
	
	/**
	 * Sets the user who posted this Tweet.
	 * 
	 * @param obj An object.
	 */
	public void setUser(TwitterUserResponse obj) {
		myUser = obj;
	}
	
	/**
	 * Gets the ID of the quoted Tweet.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("quoted_status_id_str")
	public String getQuotedStatusId() {
		return myQuotedStatusId;
	}
	
	/**
	 * Sets the ID of the quoted Tweet.
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setQuotedStatusId(String value) {
		myQuotedStatusId = value;
	}
	
	/**
	 * Indicates whether this is a Quoted Tweet.
	 * 
	 * @return Returns {@code true} if this is a Quoted Tweet, {@code false}
	 *         otherwise.
	 */
	@JsonProperty("is_quote_status")
	public boolean isQuoteStatus() {
		return myQuoteStatus;
	}
	
	/**
	 * Sets whether this is a Quoted Tweet.
	 * 
	 * @param value Whether this is a Quoted Tweet.
	 */
	public void setQuoteStatus(boolean value) {
		myQuoteStatus = value;
	}

	/**
	 * Gets approximately how many times this Tweet has been quoted by Twitter
	 * users.
	 * 
	 * @return The value.
	 */
	@JsonProperty("quote_count")
	public Integer getQuoteCount() {
		return myQuoteCount;
	}
	
	/**
     * Sets approximately how many times this Tweet has been quoted by Twitter
	 * users.
	 * 
	 * @param value A value.
	 */
	public void setQuoteCount(Integer value) {
		myQuoteCount = value;
	}
	
	/**
	 * Gets the number of times this Tweet has been replied to.
	 * 
	 * @return The value.
	 */
	@JsonProperty("reply_count")
	public int getReplyCount() {
		return myReplyCount;
	}
	
	/**
     * Sets the number of times this Tweet has been replied to.
	 * 
	 * @param value A value.
	 */
	public void setReplyCount(int value) {
		myReplyCount = value;
	}
	
	/**
	 * Gets the number of times this Tweet has been retweeted.
	 * 
	 * @return The value.
	 */
	@JsonProperty("retweet_count")
	public int getRetweetCount() {
		return myRetweetCount;
	}
	
	/**
     * Sets the number of times this Tweet has been retweeted.
	 * 
	 * @param value A value.
	 */
	public void setRetweetCount(int value) {
		myRetweetCount = value;
	}
	
	/**
	 * Gets approximately how many times this Tweet has been liked by Twitter
	 * users.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("favorite_count")
	public Integer getFavoriteCount() {
		return myFavoriteCount;
	}
	
	/**
   	 * Sets approximately how many times this Tweet has been liked by Twitter
	 * users.
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setFavoriteCount(Integer value) {
		myFavoriteCount = value;
	}
	
	/**
	 * Gets the entities which have been parsed out of the text of this Tweet.
	 * 
	 * @return The object. May be {@code null}.
	 */
	public TwitterEntitiesResponse getEntities() {
		return myEntities;
	}
	
	/**
	 * Sets the entities which have been parsed out of the text of this Tweet.
	 * 
	 * @param obj An object. May be {@code null}.
	 */
	public void setEntities(TwitterEntitiesResponse obj) {
		myEntities = obj;
	}
	
	/**
	 * Gets the extended entities which have been attached to this Tweet.
	 * 
	 * @return The object. May be {@code null}.
	 */
	@JsonProperty("extended_entities")
	public TwitterExtendedEntitiesResponse getExtendedEntities() {
		return myExtEntities;
	}
	
	/**
	 * Sets the extended entities which have been attached to this Tweet.
	 * 
	 * @param obj obj An object. May be {@code null}.
	 */
	public void setExtendedEntities(TwitterExtendedEntitiesResponse obj) {
		myExtEntities = obj;
	}
	
	/**
	 * Indicates whether this Tweet has been liked by the authenticated user.
	 * 
	 * @return Returns {@code true} if this Tweet has been liked by the
	 *         authenticated user, {@code false} otherwise. May be {@code null}.
	 */
	public Boolean isFavorited() {
		return myFavorited;
	}
	
	/**
	 * Sets whether this Tweet has been liked by the authenticated user.
	 * 
	 * @param value Whether this Tweet has been liked by the authenticated user.
	 *              May be {@code null}.
	 */
	public void setFavorited(Boolean value) {
		myFavorited = value;
	}
	
	/**
	 * Indicates whether this Tweet has been retweeted by the authenticated
	 * user.
	 * 
	 * @return Returns {@code true} if this Tweet has been retweeted by the
	 *         authenticated user, {@code false} otherwise.
	 */
	public Boolean isRetweeded() {
		return myRetweeted;
	}
	
	/**
	 * Sets whether this Tweet has been retweeted by the authenticated user.
	 * 
	 * @param value Whether this Tweet has been retweeded by the authenticated
	 *              user.
	 */
	public void setRetweeded(Boolean value) {
		myRetweeted = value;
	}
	
	/**
	 * Indicates whether the URL contained in this Tweet may contain content or
	 * media identified as sensitive content.
	 * 
	 * @return Returns {@code true} if the URL contained in this Tweet may
	 *         contain content or media identified as sensitive content,
	 *         {@code false} otherwise. May be {@code null}.
	 */
	@JsonProperty("possible_sensitve")
	public Boolean isPossiblySensitive() {
		return myPossiblySensitive;
	}
	
	/**
	 * Sets whether the URL contained in this Tweet may contain content or media
	 * identified as sensitive content.
	 * 
	 * @param value Whether the URL contained in this Tweet may contain content
	 *              or media identified as sensitive content. May be
	 *              {@code null}.
	 */
	public void setPossibilySensitive(Boolean value) {
		myPossiblySensitive = value;
	}
	
	/**
	 * Gets the maximum value of the {@code filter_level} parameter which may be
	 * used and still stream this Tweet.
	 * 
	 * @return The value.
	 */
	@JsonProperty("filter_level")
	public TwitterFilterLevel getFilterLevel() {
		return myFilterLevel;
	}
	
	/**
	 * Sets the maximum value of the {@code filter_level} parameter which may be
	 * used and still stream this Tweet.
	 * 
	 * @param value A value.
	 */
	public void setFilterLevel(TwitterFilterLevel value) {
		myFilterLevel = value;
	}
	
	/**
	 * Gets the machine-detected language of this Tweet text.
	 * 
	 * @return The value (BCP 47 code or {@code und}). May be {@code null}.
	 */
	public String getLang() {
		return myLang;
	}
	
	/**
	 * Sets the machine-detected language of this Tweet text.
	 * 
	 * @return A value (BCP 47 code or {@code und}). May be {@code null}.
	 */
	public void setLang(String value) {
		myLang = value;
	}
}
