package it.scoppelletti.sunshine.twitter;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import it.scoppelletti.sunshine.SpringExt;
import it.scoppelletti.sunshine.twitter.model.TwitterStreamingRequest;
import it.scoppelletti.sunshine.twitter.service.TwitterStreamingService;
import it.scoppelletti.sunshine.twitter.test.Application;

@Slf4j
@Import(StreamingMessageCounter.class)
@ActiveProfiles(SpringExt.PROFILE_TEST)
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class StreamingServiceTest {

	@Autowired
	private TwitterStreamingService myService;
	
	@Autowired
	private StreamingMessageCounter myCounter;
	
	public StreamingServiceTest() {
	}
	
	@Before
	public void setUp() {
		myCounter.resetCounter();
	}
	
	@Test
	public void test() throws Exception {
		myService.connect(new TwitterStreamingRequest()
				.addTerm("music"));
		
		Thread.sleep(2000L);
		
		myService.disconnect();
		myLogger.info("messageCount={}", myCounter.getCounter());
	}
}
