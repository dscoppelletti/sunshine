package it.scoppelletti.sunshine.twitter;

import java.util.concurrent.atomic.AtomicInteger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import it.scoppelletti.sunshine.twitter.service.TwitterStatusUpdateEvent;

@Component
public class StreamingMessageCounter implements InitializingBean {

	private AtomicInteger myCounter;
	
	public StreamingMessageCounter() {
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		myCounter = new AtomicInteger();
	}
	
	public void resetCounter() {
		myCounter.set(0);
	}
	
	public int getCounter() {
		return myCounter.get();
	}
	
	@Async
	@EventListener
	public void MessageHandler(TwitterStatusUpdateEvent event) {
		myCounter.incrementAndGet();
	}
}
