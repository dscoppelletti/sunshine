package it.scoppelletti.sunshine.firebase.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assume;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import it.scoppelletti.sunshine.SpringExt;
import it.scoppelletti.sunshine.firebase.model.FcmMessage;
import it.scoppelletti.sunshine.firebase.model.FcmMessageRequest;
import it.scoppelletti.sunshine.firebase.model.FcmMessageResponse;
import it.scoppelletti.sunshine.firebase.model.FcmMessageToDevice;
import it.scoppelletti.sunshine.firebase.model.FcmNotification;
import it.scoppelletti.sunshine.firebase.test.Application;

@Slf4j
@ActiveProfiles(SpringExt.PROFILE_TEST)
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class FcmServiceTest {

	@Value("${it.scoppelletti.test.fcm.registrationToken:}")
	private String myRegistrationToken;
	
	@Autowired
	private FcmService myFcmService;
	
	public FcmServiceTest() {
	}
	
	@Test
	public void sendToDevice() throws Exception {
		FcmMessage msg;
		FcmNotification notification;
		FcmMessageRequest req;
		FcmMessageResponse resp;
		
		Assume.assumeFalse("No registration token provided",
				StringUtils.isBlank(myRegistrationToken));
		
		myLogger.info("registration token: {}.", myRegistrationToken);
		msg = new FcmMessageToDevice(myRegistrationToken);
		notification = new FcmNotification();
		notification.setTitle("sendToDevice");
		notification.setBody(FcmServiceTest.class.getName());
		msg.setNotification(notification);
		
		req = new FcmMessageRequest(msg);
		myLogger.info("Sending message {}.", req);
		resp = myFcmService.send(req);
		myLogger.info("Received response {}", resp);
	}
}
