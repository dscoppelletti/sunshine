package it.scoppelletti.sunshine.firebase.types;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import it.scoppelletti.sunshine.SpringExt;
import it.scoppelletti.sunshine.firebase.test.Application;

@ActiveProfiles(SpringExt.PROFILE_TEST)
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class DurationTest {

	public DurationTest() {
	}
	
	@Test
	public void formatTest() throws Exception {
		FirebaseDuration x;
		
		x = new FirebaseDuration(3, 0);
		MatcherAssert.assertThat(x.toString(), Matchers.equalTo("3s"));
		
		x = new FirebaseDuration(3, 1);
		MatcherAssert.assertThat(x.toString(),
				Matchers.equalTo("3.000000001s"));
	}
	
	@Test
	public void parseTest() throws Exception {
		FirebaseDuration x;
		
		x = FirebaseDuration.fromString("3.5s");
		MatcherAssert.assertThat(x.getSeconds(), Matchers.equalTo(3L));
		MatcherAssert.assertThat(x.getNanos(), Matchers.equalTo(500000000));
	}
}
