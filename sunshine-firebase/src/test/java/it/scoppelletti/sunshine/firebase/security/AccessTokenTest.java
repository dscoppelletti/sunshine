package it.scoppelletti.sunshine.firebase.security;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Assume;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import it.scoppelletti.sunshine.SpringExt;
import it.scoppelletti.sunshine.firebase.test.Application;

@Slf4j
@ActiveProfiles(SpringExt.PROFILE_TEST)
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class AccessTokenTest {
	
	@Value("${it.scoppelletti.test.accessToken:false}")
	private boolean myEnabled;
	
	@Autowired
	private GoogleCredential myCredential;
	
	public AccessTokenTest() {
	}
	
	@Test
	public void test() throws Exception {
		String accessToken, accessToken2;

		Assume.assumeTrue("Test disabled.", myEnabled);
		
		myCredential.refreshToken();
		accessToken = myCredential.getAccessToken();
		myLogger.info("accessToken={}", accessToken);
		
		myCredential.refreshToken();
		accessToken2 = myCredential.getAccessToken();
		myLogger.info("accessToken={}", accessToken2);
		
		MatcherAssert.assertThat(accessToken2, Matchers.equalTo(accessToken));
	}
}
