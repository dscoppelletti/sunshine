/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase.service;

import java.io.IOException;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import it.scoppelletti.sunshine.http.HttpExt;

/**
 * Sets the authorization header for
 * <abbr title="Firebase Cloud Messaging">FCM</abbr>.
 * 
 * @see   <a href="http://firebase.google.com/docs/cloud-messaging/auth-server"
 *        target="_blank">Authorize Send Requests</a>
 * @since 1.0.0
 */
public class FcmAuthorize implements ClientHttpRequestInterceptor {
	private final GoogleCredential myCredential;
	
	/**
	 * Constructor.
	 * 
	 * @param credential Credential to call Google APIs.
	 */
	public FcmAuthorize(GoogleCredential credential) {
		if (credential == null) {
			throw new NullPointerException("Argument credential is null.");
		}
		
		myCredential = credential;
	}
	
	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body,
			ClientHttpRequestExecution execution) throws IOException {
		String value;
		HttpHeaders headers;
		
		headers = request.getHeaders();
		
		myCredential.refreshToken();
		value = new StringBuilder(HttpExt.AUTH_BEARER)
				.append(' ')
				.append(myCredential.getAccessToken()).toString();
		headers.add(HttpExt.HEADER_AUTH, value);
		return execution.execute(request, body);
	}
}
