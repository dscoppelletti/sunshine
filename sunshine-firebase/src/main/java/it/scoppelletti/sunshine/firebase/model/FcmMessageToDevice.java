/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase.model;

import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

/**
 * Message to send to a device.
 * 
 * @see <a href="http://firebase.google.com/docs/cloud-messaging/send-message#send_messages_to_specific_devices"
 *      target="_blank">Send messages to specific devices</a>
 * @see <a href="http://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages"
 *      target="_blank">REST Resource: projects.messages</a>
 * @since 1.0.0
 */
@ToString(callSuper = true)
public class FcmMessageToDevice extends FcmMessage {
	private final String myToken;
	
	/**
	 * Constructor.
	 * 
	 * @param token The registration token of the target device.
	 */
	public FcmMessageToDevice(String token) {
		if (StringUtils.isBlank(token)) {
			throw new NullPointerException("Argument token is null.");
		}
		
		myToken = token;
	}
	
	/**
	 * Gets the registration token of the target device.
	 * 
	 * @return The value.
	 */
	public String getToken() {
		return myToken;
	}
}
