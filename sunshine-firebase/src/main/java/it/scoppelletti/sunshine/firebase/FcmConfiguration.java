/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import it.scoppelletti.sunshine.firebase.service.DefaultFcmService;
import it.scoppelletti.sunshine.firebase.service.FcmAuthorize;
import it.scoppelletti.sunshine.firebase.service.FcmService;

/**
 * Configuration for <abbr title="Firebase Cloud Messaging">FCM</abbr>.
 * 
 * @since 1.0.0
 */
@Configuration
public class FcmConfiguration {
	
	/**
	 * Base url of FCM APIs.
	 */
	public static final String BASE_URL =
			"https://fcm.googleapis.com/v1/projects/";
	
	/**
	 * Name of the bean implementing the {@code RestTemplate} component.
	 */
	public static final String BEAN_RESTTEMPLATE =
			"it-scoppelletti-firebase-cloudmessaging-restTemplate";
	
	@Autowired 
	private FirebaseProperties myProps;
	
	@Autowired
	private GoogleCredential myCredential;
	
	/**
	 * Sole constructor.
	 */
	public FcmConfiguration() {
	}
	
	/**
	 * Defines the service for sending messages through FCM server.
	 * 
	 * @return The bean.
	 */
	@Bean
	@ConditionalOnMissingBean(FcmService.class)
	public FcmService fcmService() {
		return new DefaultFcmService();
	}
	
	/**
	 * Defines the {@code RestTemplate} component used for JWT authentication.
	 * 
	 * @param  builder The builder.
	 * @return         The bean.
	 */
	@Bean(name = FcmConfiguration.BEAN_RESTTEMPLATE)
	@ConditionalOnMissingBean(name = FcmConfiguration.BEAN_RESTTEMPLATE)
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		String baseUrl;
		
		baseUrl = new StringBuilder(FcmConfiguration.BASE_URL)
				.append(myProps.getProjectId())
				.toString();
		return builder
				.rootUri(baseUrl)
				.additionalInterceptors(new FcmAuthorize(myCredential))
				.build();
	}
}
