/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase.model;

import java.util.Map;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;
import it.scoppelletti.sunshine.firebase.FcmAndroidMessagePriority;
import it.scoppelletti.sunshine.firebase.types.FirebaseDuration;

/**
 * Android specific options for messages sent through
 * <abbr title="Firebase Cloud Messaging">FCM</abbr> connection server.
 * 
 * @see <a href="http://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#androidconfig"
 *      target="_blank">REST Resource: projects.messages</a>
 * @since 1.0.0
 */
@ToString
public class FcmAndroidConfig {
	private String myCollapseKey;
	private FcmAndroidMessagePriority myPriority;
	private FirebaseDuration myTimeToLive;
	private String myRestrictedPackageName;
	private Map<String, String> myData;
	private FcmAndroidNotification myNotification;
	
	/**
	 * Sole constructor.
	 */
	public FcmAndroidConfig() {
	}
	
	/**
	 * Gets the identifier of a group of messages that can be collapsed, so that
	 * only the last message gets sent when delivery can be resumed.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("collapse_key")
	public String getCollapseKey() {
		return myCollapseKey;
	}
	
	/**
	 * Sets an identifier of a group of messages that can be collapsed, so that
	 * only the last message gets sent when delivery can be resumed.
	 * 
	 * <p>A maximum of 4 different collapse keys is allowed at any given
	 * time.</p>
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setCollapseKey(String value) {
		myCollapseKey = value;
	}
	
	/**
	 * Gets the message priority.
	 * 
	 * @return The value. May be {@code null}.
	 */
	public FcmAndroidMessagePriority getPriority() {
		return myPriority;
	}
	
	/**
	 * Sets the message priority.
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setPriority(FcmAndroidMessagePriority value) {
		myPriority = value;
	}
	
	/**
	 * Gets how long the message should be kept in FCM storage if the device is
	 * offline. 
	 * 
	 * @return The object. May be {@code null}.
	 */
	@JsonProperty("ttl")
	public FirebaseDuration getTimeToLive() {
		return myTimeToLive;
	}
	
	/**
	 * Sets how long the message should be kept in FCM storage if the device is
	 * offline. 
	 * 
	 * <p>The maximum time to live supported is 4 weeks, and the default value
	 * is 4 weeks if not set. The value will be rounded down to the nearest
	 * second.</p>
	 * 
	 * @param obj An object. May be {@code null}.
	 */
	public void setTimeToLive(FirebaseDuration obj) {
		myTimeToLive = obj;
	}
	
	/**
	 * Gets the package name of the application where the registration tokens
	 * must match in order to receive the message.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("restricted_package_name")
	public String getRestrictedPackageName() {
		return myRestrictedPackageName;
	}
	
	/**
	 * Sets the package name of the application where the registration tokens
	 * must match in order to receive the message.
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setRestrictedPackageName(String value) {
		myRestrictedPackageName = value;
	}
	
	/**
	 * Gets the arbitrary key/value payload.
	 * 
	 * @return The collection. May be {@code null}.
	 */
	public Map<String, String> getData() {
		return myData;
	}
	
	/**
	 * Sets an arbitrary key/value payload.
	 * 
	 * <p>If set, will override the payload set in the enclosing
	 * {@code FCMMessage} object.</p>
	 * 
	 * @param obj A collection. May be {@code null}.
	 * @see       it.scoppelletti.sunshine.firebase.model.FcmMessage#setData
	 */
	public void setData(Map<String, String> obj) {
		myData = obj;
	}
	
	/**
	 * Gets the notification.
	 * 
	 * @return The object. May be {@code null}.
	 */
	public FcmAndroidNotification getNotification() {
		return myNotification;
	}
	
	/**
	 * Sets the notification.
	 * 
	 * @param obj An object. May be {@code null}.
	 */
	public void setNotification(FcmAndroidNotification obj) {
		myNotification = obj;
	}
}
