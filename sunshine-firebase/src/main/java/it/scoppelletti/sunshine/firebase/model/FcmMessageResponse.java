/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase.model;

import lombok.ToString;

/**
 * Response from sending a message.
 * 
 * @see   <a href="http://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages/send#response-body"
 *        target="_blank">Response body</a>
 * @since 1.0.0
 */
@ToString
public class FcmMessageResponse {
	private String myName;
	
	/**
	 * Sole constructor.
	 */
	public FcmMessageResponse() {
	}
	
	/**
	 * Gets the identifier of the message sent.
	 * 
	 * @return The value in the format
	 *         <code>projects/{projectId}/messages/{messageId}</code>.
	 */
	public String getName() {
		return myName;
	}
	
	/**
	 * Sets the identifier of the message sent.
	 * 
	 * @param value A value.
	 */
	public void setName(String value) {
		myName = value;
	}
}
