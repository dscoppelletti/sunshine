/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase.model;

import lombok.ToString;

/**
 * Basic notification template to use across all platforms.
 * 
 * @see <a href="http://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#Notification"
 *      target="_blank">REST Resource: projects.messages</a>
 * @since 1.0.0
 */
@ToString
public class FcmNotification {
	private String myTitle;
	private String myBody;
	
	/**
	 * Sole constructor.
	 */
	public FcmNotification() {
	}
	
	/**
	 * Gets the title.
	 * 
	 * @return The value. May be {@code null}.
	 */
	public String getTitle() {
		return myTitle;
	}
	
	/**
	 * Sets the title.
	 * 
	 * <p>May be overridden by the title set in the platform specific
	 * template.</p>
	 * 
	 * @param value A value. May be {@code null}.
	 * @see it.scoppelletti.sunshine.firebase.model.FcmAndroidNotification#setTitle
	 */
	public void setTitle(String value) {
		myTitle = value;
	}
	
	/**
	 * Gets the body text.
	 * 
	 * @return The value. May be {@code null}.
	 */
	public String getBody() {
		return myBody;
	}
	
	/**
	 * Sets the body text.
	 * 
	 * <p>May be overridden by the body text set in the platform specific
	 * template.</p>
	 * 
	 * @param value A value. May be {@code null}.
	 * @see it.scoppelletti.sunshine.firebase.model.FcmAndroidNotification#setBody
	 */
	public void setBody(String value) {
		myBody = value;
	}
}
