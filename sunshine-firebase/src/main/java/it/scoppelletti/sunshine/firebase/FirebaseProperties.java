/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase;

import java.util.ArrayList;
import java.util.List;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

/**
 * Configuration properties for Firebase.
 * 
 * @since 1.0.0
 */
@Slf4j
@Validated
@ToString(exclude = "myProjectId")
@ConfigurationProperties(prefix = FirebaseProperties.PREFIX)
public class FirebaseProperties implements InitializingBean {

	/**
	 * The name prefix of the properties.
	 */
	public static final String PREFIX = "it.scoppelletti.firebase";
	
	/**
	 * Scope to authorize access to FCM Server.
	 */
	public static final String SCOPE_FCM =
			"https://www.googleapis.com/auth/firebase.messaging";
	
	private String myProjectId;
	private String myServiceAccountLocation;
	private List<String> myScopes;
	
	/**
	 * Sole constructor.
	 */
	public FirebaseProperties() {
		myScopes = new ArrayList<>(1);
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		myLogger.info(toString());
	}
	
	/**
	 * Gets the project ID.
	 * 
	 * @return The value.
	 */
	@NotBlank
	public String getProjectId() {
		return myProjectId;
	}
	
	/**
	 * Sets the project ID.
	 * 
	 * @param value A value.
	 */
	public void setProjectId(String value) {
		myProjectId = value;
	}
	
	/**
	 * Gets the location of the service account's credentials.
	 * 
	 * @return The value.
	 */
	@NotBlank
	public String getServiceAccountLocation() {
		return myServiceAccountLocation;
	}
	
	/**
	 * Sets the location of he service account's credentials.
	 * 
	 * @param value A value.
	 * @see   <a href="http://firebase.google.com/docs/admin/setup"
	 *        target="_blank">Add the Firebase Admin SDK to Your Server</a>
	 */
	public void setServiceAccountLocation(String value) {
		myServiceAccountLocation = value;
	}
	
	/**
	 * Gets the scopes for access authorization.
	 * 
	 * @return The collection.
	 */
	public List<String> getScopes() {
		return myScopes;
	}
}
