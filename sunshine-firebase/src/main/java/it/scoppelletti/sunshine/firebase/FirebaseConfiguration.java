/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import it.scoppelletti.sunshine.io.IOExt;

/**
 * Configuration for Firebase.
 * 
 * @since 1.0.0
 */
@Configuration
public class FirebaseConfiguration {
	// GoogleCredential from Google API Client 1.23.0
	// GoogleCredentials from Google Auth Library 0.8.0
	// It's a pain!
	
	@Autowired 
	private FirebaseProperties myProps;
	
	@Autowired
	private ResourceLoader myResLoader;
	
	/**
	 * Sole constructor.
	 */
	public FirebaseConfiguration() {
	}
	
	/**
	 * Defines the credential to call Google APIs.
	 * 
	 * @return The bean.
	 * @see    <a href="http://developers.google.com/api-client-library/java/google-api-java-client/oauth2"
	 * 		   target="_blank">Using OAuth 2.0 with the Google API Client
	 *         Library for Java</a>
	 */
	@Bean
	@ConditionalOnMissingBean(GoogleCredential.class)
	public GoogleCredential googleCredential() {
		Resource res;
		InputStream in = null;
		GoogleCredential credential;
		
		res = myResLoader.getResource(myProps.getServiceAccountLocation());
		
		try {
			if (!res.exists()) {
				throw new FileNotFoundException(String.format(
						"Resource %1$s not found.",
						myProps.getServiceAccountLocation()));
			}
			
			in = res.getInputStream();
			credential = GoogleCredential.fromStream(in)
					.createScoped(myProps.getScopes());
		} catch (IOException ex) {
			throw new UncheckedIOException(ex);
		} finally {
			in = IOExt.close(in);
		}
		
		return credential;
	}
	
	/**
	 * Defines the Firebase application.
	 * 
	 * @return The bean.
	 * @see    <a href="http://firebase.google.com/docs/admin/setup"
	 *         target="_blank">Add the Firebase Admin SDK to Your Server</a>
	 */
	@Bean
	@ConditionalOnMissingBean(FirebaseApp.class)
	public FirebaseApp firebaseApp() {
		String databaseUrl;
		Resource res;
		InputStream in = null;
		FirebaseOptions opts;
		GoogleCredentials credentials;
		
		res = myResLoader.getResource(myProps.getServiceAccountLocation());
		
		try {
			if (!res.exists()) {
				throw new FileNotFoundException(String.format(
						"Resource %1$s not found.",
						myProps.getServiceAccountLocation()));
			}
			
			in = res.getInputStream();
			credentials = GoogleCredentials.fromStream(in);
		} catch (IOException ex) {
			throw new UncheckedIOException(ex);
		} finally {
			in = IOExt.close(in);
		}
		
		databaseUrl = new StringBuilder("https://")
				.append(myProps.getProjectId())
				.append(".firebaseio.com/").toString();
		
		opts = new FirebaseOptions.Builder()
				.setProjectId(myProps.getProjectId())
				.setCredentials(credentials)
				.setDatabaseUrl(databaseUrl)
				// .setJsonFactory(null)
				// .setThreadManager(null)
				.build();
		return FirebaseApp.initializeApp(opts);
	}
}
