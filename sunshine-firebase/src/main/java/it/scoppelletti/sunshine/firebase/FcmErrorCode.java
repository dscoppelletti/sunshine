/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase;

/**
 * Error codes for <abbr title="Firebase Cloud Messaging">FCM</abbr> failure
 * conditions.
 * 
 * @see <a href="http://firebase.google.com/docs/reference/fcm/rest/v1/ErrorCode"
 *      target="_blank">ErrorCode</a>
 * @since 1.0.0
 */
public enum FcmErrorCode {

	/**
	 * No more information is available about this error.
	 */
	UNSPECIFIED_ERROR,
	
	/**
	 * Request parameters were invalid.
	 */
	INVALID_ARGUMENT,
	// TODO - An extension of type google.rpc.BadRequest is returned to specify
	// which field was invalid.
	
	/**
	 * App instance was unregistered from FCM. This usually means that the token
	 * used is no longer valid and a new one must be used.
	 */
	UNREGISTERED,
	
	/**
	 * The authenticated sender ID is different from the sender ID for the
	 * registration token.
	 */
	SENDER_ID_MISMATCH,
	
	/**
	 * Sending limit exceeded for the message target.
	 */
	QUOTA_EXCEEDED,
	// TODO - An extension of type google.rpc.QuotaFailure is returned to
	// specify which quota got exceeded.
	
	/**
	 * APNs certificate or auth key was invalid or missing.
	 */
	APNS_AUTH_ERROR,
	
	/**
	 * The server is overloaded.
	 */
	UNAVAILABLE,
	
	/**
	 * An unknown internal error occurred.
	 */
	INTERNAL
}
