/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase.model;

import java.util.Map;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

/**
 * Message to send by <abbr title="Firebase Cloud Messaging">FCM</abbr> service.
 * 
 * @see <a href="http://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages"
 *      target="_blank">REST Resource: projects.messages</a>
 * @since 1.0.0
 */
@ToString
public abstract class FcmMessage {
	private Map<String, String> myData;
	private FcmNotification myNotification;
	private FcmAndroidConfig myAndroidCfg;
	
	/**
	 * Sole constructor.
	 */
	public FcmMessage() {
	}
	
	/**
	 * Gets the arbitrary key/value payload.
	 * 
	 * @return The collection. May be {@code null}.
	 */
	public Map<String, String> getData() {
		return myData;
	}
	
	/**
	 * Sets an arbitrary key/value payload.
	 * 
	 * <p>May be overridden by the payload set in the enclosed platform specific
	 * options.</p>
	 * 
	 * @param obj A collection. May be {@code null}.
	 * @see   it.scoppelletti.sunshine.firebase.model.FcmAndroidConfig#setData
	 */
	public void setData(Map<String, String> obj) {
		myData = obj;
	}
	
	/**
	 * Gets the basic notification template to use across all platforms.
	 * 
	 * @return The object. May be {@code null}.
	 */
	public FcmNotification getNotification() {
		return myNotification;
	}
	
	/**
	 * Sets the basic notification template to use across all platforms.
	 * 
	 * @param obj An object. May be {@code null}.
	 */
	public void setNotification(FcmNotification obj) {
		myNotification = obj;
	}
	
	/**
	 * Gets the Android specific options for messages sent through FCM
	 * connection server.
	 * 
	 * @return The object. May be {@code null}.
	 */
	@JsonProperty("android")
	public FcmAndroidConfig getAndroidConfig() {
		return myAndroidCfg;
	}
	
	/**
	 * Sets the Android specific options for messages sent through FCM
	 * connection server.
	 * 
	 * @param obj An object. May be {@code null}.
	 */
	public void setAndroidConfig(FcmAndroidConfig obj) {
		myAndroidCfg = obj;
	}
}
