/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

/**
 * Notification to send to Android devices.
 * 
 * @see <a href="http://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#AndroidNotification"
 *      target="_blank">REST Resource: projects.messages</a>
 * @since 1.0.0
 */
@ToString
public class FcmAndroidNotification {
	private String myTitle;
	private String myBody;
	private String myIcon;
	private String myColor;
	private String mySound;
	private String myTag;
	private String myClickAction;
	private String myBodyLocKey;
	private List<String> myBodyLocArgs;
	private String myTitleLocKey;
	private List<String> myTitleLocArgs;
	
	/**
	 * Sole constructor.
	 */
	public FcmAndroidNotification() {
	}
	
	/**
	 * Gets the title.
	 * 
	 * @return The value. May be {@code null}.
	 * @see    #getTitleLocKey
	 */
	public String getTitle() {
		return myTitle;
	}
	
	/**
	 * Sets the title.
	 * 
	 * <p>If set, it will override the title set in the notification template to
	 * use across all platforms.</p>
	 * 
	 * @param value A value. May be {@code null}.
	 * @see   #setTitleLocKey
	 * @see   it.scoppelletti.sunshine.firebase.model.FcmNotification#setTitle
	 */
	public void setTitle(String value) {
		myTitle = value;
	}
	
	/**
	 * Gets the body text.
	 * 
	 * @return The value. May be {@code null}.
	 * @see    #getBodyLocKey
	 */
	public String getBody() {
		return myBody;
	}
	
	/**
	 * Sets the body text.
	 * 
     * <p>If set, it will override the body text set in the notification
     * template to use across all platforms.</p>
	 * 
	 * @param value A value. May be {@code null}.
	 * @see   #setBodyLocKey
	 * @see   it.scoppelletti.sunshine.firebase.model.FcmNotification#setBody
	 */
	public void setBody(String value) {
		myBody = value;
	}
	
	/**
	 * Gets the icon.
	 * 
	 * @return The value as the name of a drawable resource of the client App.
	 *         May be {@code null}.
	 * @see    #getColor
	 */
	public String getIcon() {
		return myIcon;
	}
	
	/**
	 * Sets the icon.
	 * 
	 * @param value A value as the name of a drawable resource of the client
	 *              App. May be {@code null}.
	 * @see         #setColor
	 */
	public void setIcon(String value) {
		myIcon = value;
	}
	
	/**
	 * Gets the icon color.
	 * 
	 * @return The value in {@code #RRGGBB} format. May be {@code null}.
	 * @see    #getIcon
	 */
	public String getColor() {
		return myColor;
	}
	
	/**
	 * Sets the icon color.
	 * 
	 * @param value A value in {@code #RRGGBB} format. May be {@code null}.
	 * @see         #setIcon
	 */
	public void setColor(String value) {
		myColor = value;
	}
	
	/**
	 * Gets the sound to play when the device receives the notification.
	 * 
	 * @return The value as a file name in {@code /res/raw/} resources folder of
	 *         the client App. May be {@code null}.
	 */
	public String getSound() {
		return mySound;
	}
	
	/**
	 * Sets the sound to play when the device receives the notification.
	 * 
	 * @param value A value as a file name in {@code /res/raw/} resources folder
	 *              of the client App. May be {@code null}.
	 */
	public void setSound(String value) {
		mySound = value;
	}
	
	/**
	 * Gets the identifier used to replace existing notifications in the
	 * notification drawer.
	 * 
	 * @return The value. May be {@code null}.
	 */
	public String getTag() {
		return myTag;
	}
	
	/**
	 * Sets the identifier used to replace existing notifications in the
	 * notification drawer.
	 * 
	 * <p>If not specified, each request creates a new notification. If
	 * specified and a notification with the same tag is already being shown,
	 * the new notification replaces the existing one in the notification
	 * drawer.</p>
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setTag(String value) {
		myTag = value;
	}
	
	/**
	 * Gets the action associated with a user click on the notification.
	 * 
	 * @return The value. May be {@code null}.
	 */
	@JsonProperty("click_action")
	public String getClickAction() {
		return myClickAction;
	}
	
	/**
	 * Sets the action associated with a user click on the notification.
	 * 
	 * <p>If specified, an activity with a matching intent filter is launched
	 * when a user clicks on the notification.</p>
	 * 
	 * @param value A value. May be {@code null}.
	 */
	public void setClickAction(String value) {
		myClickAction = value;
	}
	
	/**
	 * Gets the name of the string resource of the client App to use to localize
	 * the body text.
	 * 
	 * @return The value. May be {@code null}.
	 * @see    #getBodyLocArgs
	 */
	@JsonProperty("body_loc_key")
	public String getBodyLocKey() {
		return myBodyLocKey;
	}
	
	/**
	 * Sets the name of the string resource of the client App to use to localize
	 * the body text.
	 * 
	 * @param value A value. May be {@code null}.
	 * @see         #setBodyLocArgs
	 */
	public void setBodyLocKey(String value) {
		myBodyLocKey = value;
	}
	
	/**
	 * Gets the variable string values to be used in place of the format
	 * specifiers in {@code getBodyLocKey} string resource to use to localize
	 * the body text.
	 * 
	 * @return The collection. May be {@code null}.
	 * @see    #getBodyLocKey
	 */
	@JsonProperty("body_loc_args")
	public List<String> getBodyLocArgs() {
		return myBodyLocArgs;
	}
	
	/**
	 * Sets the variable string values to be used in place of the format
	 * specifiers in {@code setBodyLocKey} string resource to use to localize
	 * the body text.
	 * 
	 * @param obj A collection. May be {@code null}.
	 * @see       #setBodyLocKey
	 */
	public void setBodyLocArgs(List<String> obj) {
		myBodyLocArgs = obj;
	}
	
	/**
	 * Gets the name of the string resource of the client App to use to localize
	 * the title text.
	 * 
	 * @return The value. May be {@code null}.
	 * @see    #getTitleLocArgs
	 */
	@JsonProperty("title_loc_key")
	public String getTitleLocKey() {
		return myTitleLocKey;
	}
	
	/**
	 * Sets the name of the string resource of the client App to use to localize
	 * the title text.
	 * 
	 * @param value A value. May be {@code null}.
	 * @see         #setTitleLocArgs	
	 */
	public void setTitleLocKey(String value) {
		myTitleLocKey = value;
	}
	
	/**
	 * Gets the variable string values to be used in place of the format
	 * specifiers in {@code getTitleLocKey} string resource to use to localize
	 * the title text.
	 * 
	 * @return The collection. May be {@code null}.
	 * @see    #getTitleLocKey
	 */
	@JsonProperty("title_loc_args")
	public List<String> getTitleLocArgs() {
		return myTitleLocArgs;
	}	
	
	/**
	 * Sets the variable string values to be used in place of the format
	 * specifiers in {@code setTitleLocKey} string resource to use to localize
	 * the title text.
	 * 
	 * @param obj A collection. May be {@code null}.
	 * @see       #setTitleLocKey
	 */
	public void setTitleLocArgs(List<String> obj) {
		myTitleLocArgs = obj;
	}
}
