/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

/**
 * Request to send a message.
 * 
 * @see   <a href="http://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages/send#request-body"
 *        target="_blank">Request body</a>
 * @since 1.0.0
 */
@ToString
public class FcmMessageRequest {
	private final FcmMessage myMsg;
	private boolean myValidateOnly;

	/**
	 * Constructor.
	 * 
	 * @param message The message.
	 */
	public FcmMessageRequest(FcmMessage message) {
		if (message == null) {
			throw new NullPointerException("Argument message is null.");
		}
		
		myMsg = message;
	}
	
	
	/**
	 * Gets the message.
	 * 
	 * @return The message.
	 */
	public FcmMessage getMessage() {
		return myMsg;
	}
	
	/**
	 * Indicates whether the request has to be tested without actually
	 * delivering the message.
	 * 
	 * @return Returns {@code true} if the request has to be to be tested
	 *         without actually delivering the message, {@code false} if the
	 *         message has to be also delivered.
	 */
	@JsonProperty("validate_only")
	public boolean isValidateOnly() {
		return myValidateOnly;
	}
	
	/**
	 * Sets whether the request has to be tested without actually delivering the
	 * message.
	 * 
	 * @param value Whether the request has to be tested without actually
	 *              delivering the message.
	 */
	public void setValidateOnly(boolean value) {
		myValidateOnly = value;
	}
}
