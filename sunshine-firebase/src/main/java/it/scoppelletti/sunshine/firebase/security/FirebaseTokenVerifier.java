/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase.security;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import it.scoppelletti.sunshine.security.jwt.JwtTokenVerifier;

/**
 * Verifies ID tokens.
 * 
 * @see   <a href="http://firebase.google.com/docs/auth/admin/verify-id-tokens"
 *        target="_blank">Verify ID Tokens</a>
 * @since 1.0.0
 */
public class FirebaseTokenVerifier implements JwtTokenVerifier {

	/**
	 * Sole constructor.
	 */
	public FirebaseTokenVerifier() {
	}
	
	@Override
	public Map<String, Object> verify(String idToken) throws
			AuthenticationException {
		FirebaseToken token;
		
		try {
			token = FirebaseAuth.getInstance().verifyIdTokenAsync(idToken)
					.get();
		} catch (InterruptedException | ExecutionException ex) {
			throw new AuthenticationServiceException(ex.getLocalizedMessage(),
					ex);
		} catch (RuntimeException ex) {
			throw new BadCredentialsException(ex.getLocalizedMessage(), ex);
		}
		
		return token.getClaims();
	}
}
