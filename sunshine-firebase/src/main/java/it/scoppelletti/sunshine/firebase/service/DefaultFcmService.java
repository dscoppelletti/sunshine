/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase.service;

import javax.annotation.Resource;
import org.springframework.web.client.RestTemplate;
import it.scoppelletti.sunshine.firebase.FcmConfiguration;
import it.scoppelletti.sunshine.firebase.model.FcmMessageRequest;
import it.scoppelletti.sunshine.firebase.model.FcmMessageResponse;

/**
 * Default implementation of the {@code FcmService} interface.
 * 
 * @since 1.0.0
 */
public class DefaultFcmService implements FcmService {
	// TODO - How can I distinguish between FcmMessageResponse and
	// FcmMessageError?
	
	private static final String URL_SEND = "/messages:send";
	
	@Resource(name = FcmConfiguration.BEAN_RESTTEMPLATE)
	private RestTemplate myRestTemplate;
	
	/**
	 * Sole constructor.
	 */
	public DefaultFcmService() {
	}
	
	public FcmMessageResponse send(FcmMessageRequest req) {
		if (req == null) {
			throw new NullPointerException("Argument req is null.");
		}
		
		return myRestTemplate.postForObject(DefaultFcmService.URL_SEND, req,
				FcmMessageResponse.class);
	}
}
