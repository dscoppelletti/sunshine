/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;
import it.scoppelletti.sunshine.firebase.FcmErrorCode;

/**
 * Error response from sending a message.
 * 
 * @see <a href="http://firebase.google.com/docs/reference/fcm/rest/v1/FcmError"
 *      target="_blank">FcmError</a>
 * @since 1.0.0
 */
@ToString
public class FcmMessageError {
	private FcmErrorCode myErrorCode;
	
	/**
	 * Sole constructor.
	 */
	public FcmMessageError() {
	}
	
	/**
	 * Gets the error code specifying why the message failed.
	 * 
	 * @return The value.
	 */
	@JsonProperty("error_code")
	public FcmErrorCode getErrorCode() {
		return myErrorCode;
	}
	
	/**
	 * Sets the error code specifying why the message failed.
	 * 
	 * @param value A value.
	 */
	public void setErrorCode(FcmErrorCode value) {
		myErrorCode = value;
	}
}
