/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase;

/**
 * Priority of a message to send to Android devices.
 * 
 * @see <a href="http://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#AndroidMessagePriority"
 *      title="_blank">REST Resource: projects.messages</a>
 * @since 1.0.0
 */
public enum FcmAndroidMessagePriority {

	/**
	 * Default priority for data messages.
	 * 
	 * <p>Normal priority messages won't open network connections on a sleeping
	 * device, and their delivery may be delayed to conserve the battery. For
	 * less time-sensitive messages, such as notifications of new email or other
	 * data to sync, choose normal delivery priority.</p>
	 */
	NORMAL,
	
	/**
	 * Default priority for notification messages.
	 * 
	 * <p>FCM attempts to deliver high priority messages immediately, allowing
	 * the FCM service to wake a sleeping device when possible and open a
	 * network connection to your app server. Apps with instant messaging, chat,
	 * or voice call alerts, for example, generally need to open a network
	 * connection and make sure FCM delivers the message to the device without
	 * delay. Set high priority if the message is time-critical and requires the
	 * user's immediate interaction, but beware that setting your messages to
	 * high priority contributes more to battery drain compared with normal
	 * priority messages.</p>
	 */
	HIGH
}
