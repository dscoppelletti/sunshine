/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase.service;

import it.scoppelletti.sunshine.firebase.model.FcmMessageRequest;
import it.scoppelletti.sunshine.firebase.model.FcmMessageResponse;

/**
 * Rest client for <abbr title="Firebase Cloud Messaging">FCM</abbr> server.
 * 
 * @see <a href="http://firebase.google.com/docs/cloud-messaging/concept-options"
 *      target="_blank">About FCM Messages</a>
 * @see <a href="http://firebase.google.com/docs/cloud-messaging/send-message"
 *      target="_blank">Build App Server Send Requests</a>
 * @since 1.0.0
 */
public interface FcmService {

	/**
	 * Sends a message.
	 * 
	 * @param  req A request.
	 * @return     The response.
	 * @throws     org.springframework.web.client.RestClientException
	 */
	FcmMessageResponse send(FcmMessageRequest req);
}
