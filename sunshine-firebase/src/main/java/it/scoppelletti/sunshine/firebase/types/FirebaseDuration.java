/*
 * Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.scoppelletti.sunshine.firebase.types;

import java.util.Locale;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.StringUtils;

/**
 * Duration.
 * 
 * <p>A <i>duration</i> represents a signed, fixed-length span of time
 * represented as a count of seconds and fractions of seconds at nanosecond
 * resolution.</p>
 * 
 * @see <a href="http://developers.google.com/protocol-buffers/docs/reference/google.protobuf#duration"
 *      target="_blank">Duration</a>
 * @since 1.0.0
 */
public final class FirebaseDuration {
	
	/**
	 * Maximum number of seconds.
	 */
	public static final long SECONDS_MAX = 315_576_000_000L;
	
	/**
	 * Minimum number of seconds.
	 */
	public static final long SECONDS_MIN = -315_576_000_000L;
	
	/**
	 * Maximum number of nanos.
	 */
	public static final int NANOS_MAX = 999_999_999;
	
	/**
	 * Minimum number of nanos.
	 */
	public static final int NANOS_MIN = -999_999_999;
	
	private static final int NANOS_DIGITS = 9;
	private static final int NANOS_PER_SECOND = 1_000_000_000;
	private static final char DECIMAL_POINT = '.';
	private static final String SUFFIX = "s";
	private long mySeconds;
	private int myNanos;
	
	/**
	 * Constructor.
	 * 
	 * @param seconds Signed seconds of the span of time.
	 * @param nanos   Signed fractions of a second at nanosecond resolution of
	 *                the span of time.
	 */
	public FirebaseDuration(long seconds, int nanos) {
		if (seconds < FirebaseDuration.SECONDS_MIN ||
				seconds > FirebaseDuration.SECONDS_MAX) {
			throw new IllegalArgumentException(String.format(Locale.ENGLISH,
					"Seconds %1$s is not between %2$d and %3$d.", seconds,
					FirebaseDuration.SECONDS_MIN,
					FirebaseDuration.SECONDS_MAX));
		}
		if (nanos < FirebaseDuration.NANOS_MIN ||
				nanos > FirebaseDuration.NANOS_MAX) {
			throw new IllegalArgumentException(String.format(Locale.ENGLISH,
					"Nanos %1$s is not between %2$d and %3$d.", nanos,
					FirebaseDuration.NANOS_MIN, FirebaseDuration.NANOS_MAX));
		}

		if (seconds < 0L && nanos > 0) {
			seconds += 1L;
			nanos -= FirebaseDuration.NANOS_PER_SECOND;
		} else if (seconds > 0L && nanos < 0) {
			seconds -= 1L;
			nanos += FirebaseDuration.NANOS_PER_SECOND;
		}
		
		mySeconds = seconds;
		myNanos = nanos;
	}
	
	/**
	 * Gets the signed seconds of the span of time.
	 * 
	 * @return The value.
	 */
	public long getSeconds() {
		return mySeconds;
	}
	
	/**
	 * Gets the signed fractions of a second at nanosecond resolution of the
	 * span of time.
	 * 
	 * @return The value.
	 */
	public int getNanos() {
		return myNanos;
	}
	
	@Override
	@JsonValue
	public String toString() {
		int nanos, point;
		StringBuilder buf;
		
		buf = new StringBuilder().append(mySeconds);

		if (myNanos != 0) {
			point = buf.length();
			nanos = FirebaseDuration.NANOS_PER_SECOND + 
					((myNanos > 0) ? myNanos : -myNanos);
			while (nanos % 10 == 0) {
				nanos /= 10;
			}
			
			buf.append(nanos).setCharAt(point, FirebaseDuration.DECIMAL_POINT);
		}
		
		return buf.append(FirebaseDuration.SUFFIX).toString();
	}
	
	/**
	 * Converts a string to a duration value.
	 * 
	 * @param  s The string. May be {@code null}.
	 * @return   The value. May be {@code null}.
	 */
	@JsonCreator
	public static FirebaseDuration fromString(String s) {
		int end, i, nanos, point;
		long seconds;
		
		if (StringUtils.isBlank(s)) {
			return null;
		}
		
		end = s.length();
		if (StringUtils.endsWithIgnoreCase(s, FirebaseDuration.SUFFIX)) {
			end -= FirebaseDuration.SUFFIX.length();
		}
		
		point = s.indexOf(FirebaseDuration.DECIMAL_POINT);
		if (point < 0) {
			point = end;
		}
		
		seconds = Long.parseLong(s.substring(0, point));
		point++;
		if (point < end) {
			if (!Character.isDigit(s.charAt(point))) {
				throw new NumberFormatException();
			}
			
			nanos = Integer.parseInt(s.substring(point, end));
			end -= point;
			for (i = FirebaseDuration.NANOS_DIGITS; i > end; i--) {
				nanos *= 10;
			}	
			
			if (seconds < 0) {
				nanos = -nanos;
			}
		} else {
			nanos = 0;
		}
		
		return new FirebaseDuration(seconds, nanos);
	}
}
